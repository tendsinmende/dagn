//! # DAGn
//! 
//! The framework itself handles two parts. It creates a interface that tries to give real time feedback of the underlying data
//! graph. Further it handles the data graph and it's nodes. The graph is build by the `async_graph` crate. This is also the place where the main
//! `Node` trait comes from. That's the only trait that **must** be implemented by each node.
//!
//! # Interfaces
//! There is also a second trait that can be implemented by the Nodes that's called 
//!
//!
///Contains the node related abstractions, as well as all nodes.
pub mod nodes;
///Contains the interface related tools and special widget types.
pub mod interface;
///Contains abstract tools like data types and algorithms that are base on them.
pub mod abst;

///Contains config information for the whole program.
pub mod config;

fn main() {

    if cfg!(debug_assertions){
	println!("Logging!");
	simple_logger::init().unwrap();
    }else{
	simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Error)
        .init().unwrap()
    }

    neith::overwrite_config(neith::config::Config::load("neith.conf"));

    neith_marp::new_interface(None, Some("neith.conf"), |mut interface|{
	interface.get_root().write().unwrap().set_widget(interface::Interface::new());	
	while !interface.update(){
	    
	}
    });
}
