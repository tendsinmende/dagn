use std::{fs::{File, OpenOptions}, path::{Path, PathBuf}, sync::RwLock};

use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};
use serde::Deserialize;
use serde::Serialize;
use std::io::Write;

use log::warn;


lazy_static::lazy_static!{
    /// a some config. If not loaded by hand via `DagnConfig::load()` this is the default config.
    pub static ref DAGN_CONFIG: RwLock<DagnConfig> = RwLock::new(DagnConfig::default());
}

///Cpal related configuration
#[derive(Clone, Serialize, Deserialize)]
pub struct CpalConfig{
    ///Service that was saved. Might be used that the device found under `dev_idx` is the actually saved device.
    /// Might change if the device configuration of the system changes.
    pub out_service_name: String,
    ///Target sampling that should be reached.
    pub out_target_sampling: usize,
    ///Number of output channels that should be reached
    pub out_target_channels: usize,
}

impl Default for CpalConfig{
    #[cfg(unix)]
    fn default() -> Self{
	CpalConfig{
	    out_service_name: "pulse".to_string(),
	    out_target_sampling: 256,
	    out_target_channels: 2
	}
    }
    #[cfg(windows)]
    fn default() -> Self{
	panic!("No windows default CPAL config yet");
    }
}


///Main config structure
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct DagnConfig{
    pub cpal_config: CpalConfig
}

impl DagnConfig{

    ///Returns a copy of the currently active config.
    pub fn get() -> DagnConfig{
	DAGN_CONFIG.read().unwrap().clone()
    }

    ///Overwrites the current active config with this one
    pub fn set(new: DagnConfig){
	*DAGN_CONFIG.write().unwrap() = new;
    }
    
    ///Loads the config from `./dagn_config.ron` if no path was specified
    pub fn load(path: Option<PathBuf>){
	let path: PathBuf = if let Some(p) = path{
	    p
	}else{
	    PathBuf::from("dagn_config.ron")
	};

	
	let config = match File::open(path) {
            Ok(f) => {
		match from_reader(f) {
		    Ok(c) => c,
		    Err(e) => {
			warn!(
			    "Failed to parse config config with err: {}, loading defaults!",
			    e
			);
			DagnConfig::default()
		    }
		}
	    },
            Err(e) => {
                warn!(
                    "Could not find config file with er: {}, loading defaults!",
                    e
                );
                DagnConfig::default()
            }
        };

	*DAGN_CONFIG.write().unwrap() = config;
    }

    ///Saves the Config to the path if set, otherwise its saved to `./dagn_config.ron`
    pub fn save(path: Option<PathBuf>){
	let path: PathBuf = if let Some(p) = path{
	    p
	}else{
	    PathBuf::from("dagn_config.ron")
	};

	let pretty = PrettyConfig {
            depth_limit: 10,
            separate_tuple_members: true,
            enumerate_arrays: false,
            ..PrettyConfig::default()
        };

        let final_string = to_string_pretty(&*DAGN_CONFIG.read().unwrap(), pretty).expect("Failed to encode config!");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(path).unwrap();
	
        write!(file, "{}", final_string).unwrap();
    }
}
