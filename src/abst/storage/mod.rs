//! Collection of different storage types that can come in handy.
pub mod ring_buffer;
pub use ring_buffer::RingBuffer;

pub mod delay_buffer;
pub use delay_buffer::*;
