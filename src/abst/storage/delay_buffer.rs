use crate::abst::audio::AudioBuffer;
use crate::abst::audio::Sample;
use crate::abst::audio::Time;

use super::RingBuffer;

///A DelayBuffer is a special form of a ring buffer. It buffers samples up to its capacity and can return samples at time steps up to its buffer size.
pub struct DelayBuffer{
    buffer: RingBuffer<Sample>,
    buffer_time: Time,
    sample_rate: usize
}

impl DelayBuffer{
    pub fn new(buffer_time: Time, sampling_rate: usize) -> Self{
	DelayBuffer{
	    buffer: RingBuffer::new((buffer_time * sampling_rate as Time).ceil() as usize),
	    buffer_time,
	    sample_rate: sampling_rate,
	}
    }

    ///Pushes the sample of the supplied audio buffer into the delay buffer
    pub fn push_buffer(&mut self, buffer: &AudioBuffer){
	for s in buffer.samples(){
	    self.buffer.push(*s);
	}
    }
    
    ///Samples the delay buffer at `time` seconds before the last pushed sample
    pub fn sample(&self, time: Time) -> Option<Sample>{
	if time > self.buffer_time || time < 0.0{
	    return None;
	}
	let sample = (time * self.sample_rate as Time).floor() as usize;
	if let Some(e) = self.buffer.get(sample).cloned(){
	    Some(e)
	}else{
	    println!("Sample {} in buffer with len {}", sample, self.buffer.len());
	    None
	}
    }
}
