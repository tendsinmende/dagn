
///Bounded, heap allocated ring buffer.
pub struct RingBuffer<C>{
    size: usize,
    buffer: Vec<Option<C>>,
    ///Current location of the "start".
    read: usize,
    ///Current location of the "end"
    write: usize,
}

impl<C> RingBuffer<C>{
    pub fn new(capacity: usize) -> Self{

	let mut vec = Vec::with_capacity(capacity);
	for _i in 0..capacity{
	    vec.push(None);
	}
	
	RingBuffer{
	    size: capacity,
	    buffer: vec,
	    read: 0,
	    write: 0
	}
    }

    ///Pushes a new element, might overwrite if there was one in here before.
    pub fn push(&mut self, item: C){
	
	self.buffer[self.write] = Some(item);
	
	self.write = (self.write + 1) % self.size;
	//if the write pointer overtook the read pointer, advance the read pointer
	if self.write == self.read{
	    self.read = (self.read + 1) % self.size;
	}
    }

    ///Pops an element from the front of the buffer.
    pub fn pop(&mut self) -> Option<C>{
	//If the write pointer is the same as the read pointer, advance the write pointer together with the read pointer.
	if self.read == self.write{
	    self.write = (self.write + 1) % self.size;
	}
	let old_loc = self.read;
	self.read = (self.read + 1) % self.size;
	self.buffer[old_loc].take()
    }

    pub fn len(&self) -> usize{
	(self.write as isize - self.read as isize).rem_euclid(self.size as isize) as usize
    }
    
    ///Returns the element that is `idx` elements away from the current head of this buffer.
    ///
    ///Returns nothing if either the value is not set, or the idx > capacity or idx > len.
    pub fn get(&self, idx: usize) -> Option<&C>{
	if idx >= self.size{
	    return None;
	}
	self.buffer[(self.write as isize - 1 - idx as isize).rem_euclid(self.size as isize) as usize].as_ref()
    }

    pub fn get_mut(&mut self, idx: usize) -> Option<&mut C>{
	if idx >= self.size{
	    return None;
	}
	self.buffer[(self.write as isize - 1 - idx as isize).rem_euclid(self.size as isize) as usize].as_mut()
    }
}

#[test]
fn push_ring_buffer(){
    let mut buffer = RingBuffer::new(5);
    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    assert!(1 == buffer.pop().unwrap(), "Buffer pop failed: 1=1");

    //Push until overflow
    buffer.push(4);
    buffer.push(5);
    buffer.push(6);
    buffer.push(7);
    let pop = buffer.pop().unwrap();
    assert!(4 == pop, "Overflow test failed await 3 was {}", pop);
}

#[test]
fn get_ring_buffer(){
    let mut buffer = RingBuffer::new(5);
    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    let get = *buffer.get(2).unwrap(); 
    assert!( get == 1, "Failed to index correct element assumed 1 was {}", get);
    assert!(buffer.get(3).is_none(), "Failed to test for none outside of buffer");

    buffer.push(4);
    buffer.push(5);

    let get = *buffer.get(4).unwrap(); 
    assert!(get == 1, "Failed to get correct element in full buffer. Assumed 1 was {}", get);
    assert!(buffer.get(5).is_none(), "Failed to validate sampling outside of buffer");
}
