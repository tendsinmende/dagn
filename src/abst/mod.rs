///Audio related data types and algorithms
pub mod audio;
///Storage related stuff, like buffer implementations.
pub mod storage;
///Misc structures like normalized values, that makes sens in several domains.
pub mod misc;
