use neith::io::{DynamicValue, TextEditable};


///A normalized value is some value 0.0 - 1.0. They can be used to easily generate values within a range
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct NormVal(pub f64);

impl DynamicValue for NormVal{
    fn add(&mut self, addition: Self){
	self.0 += addition.0;
    }
    fn subtract(&mut self, sub: Self){
	self.0 -= sub.0;
    }
    fn get(&self) -> &Self{
	&self
    }
    fn get_mut(&mut self) -> &mut Self{
	self
    }
    ///Returns the value that is "correct" on a range [min..max] with given step_size
    fn value_for_percentage(min: &Self, max: &Self, step_size: &Self, percentage: f32) -> Self{
	//a normal is always in between 0 and 1 therfore this is already the percentage
	NormVal(percentage as f64)
    }
    ///Returns the percentage the current value has on a range of [min..max]
    fn percentage_for_value(&self, min: &Self, max: &Self) -> f32{
	((self.0 - min.0) as f32/ (max.0 - min.0) as f32).max(0.0).min(1.0)
    }
}

impl TextEditable for NormVal{
    fn to_string(&self) -> String {
	TextEditable::to_string(&self.0)
    }

    fn from_string(&mut self, string: &str) -> bool {
	//cast float to string, check if in 0-1 range, else return false
	let mut float = 0.0 as f64;
	if !float.from_string(string){
	    return false;
	}

	if float >= 0.0 && float <= 1.0{
	    self.0 = float;
	    true
	}else{
	    false
	}
    }
}

pub trait IntoNormValue{
    fn into_norm(self, min: Self, max: Self) -> NormVal;
}
macro_rules! impl_into_norm{
    ($ty:ty) => {
	impl IntoNormValue for $ty{
	    fn into_norm(self, min: Self, max: Self) -> NormVal{
		NormVal((((self as f64).max(min as f64).min(max as f64) + min as f64) / (max as f64 - min as f64)))
	    }
	}
    }
}

impl_into_norm!(f32);
impl_into_norm!(f64);
impl_into_norm!(usize);
impl_into_norm!(isize);
impl_into_norm!(u8);
impl_into_norm!(u16);
impl_into_norm!(u32);
impl_into_norm!(u64);
impl_into_norm!(u128);
