use std::ops::{Mul, Add};

use super::Sample;
use super::Time;

pub trait Channel{
    fn num_channel() -> usize;
    fn get_buffer(&self, channel_idx: usize) -> Option<&AudioBuffer>;
    fn get_buffer_mut(&mut self, channel_idx: usize) -> Option<&mut AudioBuffer>;
    ///Dissolves the Channel into its audio buffer. The Vec is guaranteed to be as long as `num_channel()`.
    fn dissolve(self) -> Vec<AudioBuffer>;
}

pub struct StereoChannel{
    pub buffers: [AudioBuffer; 2],
}

impl Channel for StereoChannel{
    fn num_channel() -> usize {
	2
    }

    fn get_buffer(&self, channel_idx: usize) -> Option<&AudioBuffer> {
	if channel_idx > 1{
	    None
	}else{
	    Some(&self.buffers[channel_idx])
	}
    }

    fn get_buffer_mut(&mut self, channel_idx: usize) -> Option<&mut AudioBuffer> {
	if channel_idx > 1{
	    None
	}else{
	    Some(&mut self.buffers[channel_idx])    
	}
    }

    fn dissolve(self) -> Vec<AudioBuffer> {
	self.buffers.to_vec()
    }
}


///Simple audio buffer with static size and sample rate.
#[derive(Clone, Debug)]
pub struct AudioBuffer{
    pub buffer: Vec<Sample>,
    sample_rate: usize,
    duration: Time,
}

impl AudioBuffer{
    pub fn new(size: usize, sample_rate: usize) -> Self{

	let duration = (1.0 / sample_rate as f64) * size as f64;
	
	AudioBuffer{
	    buffer: vec![0.0; size],
	    sample_rate,
	    duration
	}
    }

    pub fn set_sample(&mut self, location: usize, sample: Sample){
	self.buffer[location] = sample;
    }

    pub fn buffer_size(&self) -> usize{
	self.buffer.len()
    }

    pub fn sample_rate(&self) -> usize{
	self.sample_rate
    }

    ///Returns how much time this frame "plays" in the real world.
    /// So `self.frame_time() + self.duration()` should be the frame_time of the next Frame.
    pub fn duration(&self) -> Time{
	self.duration
    }
    
    ///Dissolves the audio buffer into its component (buffer, sample_rate, scheduled time).
    pub fn dissolve(self) -> (Vec<Sample>, usize){
	(self.buffer, self.sample_rate)
    }

    ///if `time < self.duration()` returns the sample at `time`
    pub fn sample_at(&self, time: Time) -> Option<Sample>{
	self.buffer.get(
	    (self.buffer.len() as Time * (time / self.duration())).ceil() as usize
	).cloned()
    }

    pub fn samples(&self) -> &[Sample]{
	&self.buffer
    }

    pub fn samples_mut(&mut self) -> &mut [Sample]{
	&mut self.buffer
    }
}

impl Add<AudioBuffer> for AudioBuffer{
    type Output = AudioBuffer;
    ///Adds as many samples as possible from `rhs` to self
    fn add(mut self, rhs: Self) -> Self::Output {
	let max = self.buffer.len().min(rhs.buffer.len());
	for i in 0..(max-1){
	    self.buffer[i] += rhs.buffer[i];
	}

	self
    }
}

impl Add<f32> for AudioBuffer{
    type Output = AudioBuffer;
    ///Adds as many samples as possible from `rhs` to self
    fn add(mut self, rhs: f32) -> Self::Output {
	for s in self.buffer.iter_mut(){
	    *s += rhs;
	}
	self
    }
}

impl Add<f64> for AudioBuffer{
    type Output = AudioBuffer;
    ///Adds as many samples as possible from `rhs` to self
    fn add(mut self, rhs: f64) -> Self::Output {
	for s in self.buffer.iter_mut(){
	    *s += rhs as f32;
	}
	self
    }
}

impl Mul<AudioBuffer> for AudioBuffer{
    type Output = AudioBuffer;
    ///Multiplies as many samples as possible from `rhs` to self
    fn mul(mut self, rhs: AudioBuffer) -> Self::Output {
	let max = self.buffer.len().min(rhs.buffer.len());
	for i in 0..(max-1){
	    self.buffer[i] *= rhs.buffer[i];
	}

	self
    }
}

impl Mul<f32> for AudioBuffer{
    type Output = AudioBuffer;
    ///Multiplies as many samples as possible from `rhs` to self
    fn mul(mut self, rhs: f32) -> Self::Output {
	for s in self.buffer.iter_mut(){
	    *s *= rhs;
	}

	self
    }
}

impl Mul<f64> for AudioBuffer{
    type Output = AudioBuffer;
    ///Multiplies as many samples as possible from `rhs` to self
    fn mul(mut self, rhs: f64) -> Self::Output {
	for s in self.buffer.iter_mut(){
	    *s *= rhs as f32;
	}

	self
    }
}
