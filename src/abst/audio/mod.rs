
pub mod audiobuffer;
pub use audiobuffer::*;

pub mod state;
pub use state::*;


///A single sample within a sound(buffer).
pub type Sample = f32;
///Represents a point in time, or some duration, in seconds.
pub type Time = f64;



///Contains all audio related control events.
///
/// `NewFrame` can be used in Clocked Nodes as trigger for values that need to be calculated
/// per AudioBuffer
#[derive(Clone, Debug)]
pub enum Clock{
    ///Send right before the first buffer is requested
    Start,
    ///Send after the last buffer is send.
    Stop,
    ///Similar to stop, but with the option to continue. For most nodes that are buffering something this means that they shouldn't clean
    /// their buffers, since the content might be used again when an start event is send.
    Pause,
    ///When a new frame was requested
    NewFrame
}

