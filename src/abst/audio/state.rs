use super::Clock;


///State that can be used in audio nodes. 
#[derive(Clone, Debug, Copy, PartialEq, Eq)]
pub enum AudioState{
    Paused,
    Stopped,
    Playing
}

impl AudioState{
    ///Transforms the State information into a clock signal. The signal is for instance emitted when the state changes to the current one.
    ///
    /// The transformation is like this:
    ///
    /// - Paused -> Pause
    /// - Stopped -> Stop
    /// - Playing -> Start
    pub fn into_clock(&self) -> Clock{
	match self{
	    AudioState::Paused => Clock::Pause,
	    AudioState::Stopped => Clock::Stop,
	    AudioState::Playing => Clock::Start
	}
    }
}

