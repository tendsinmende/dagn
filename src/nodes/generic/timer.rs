//! # Timer
//! The time module has two nodes. TimerTick, and the TimerReg. Tick produces an time value when it is called. The TimerReg. listens
//! on its second output for the event produced by TimerTick (or any other node that sends out an `std::time::Instant`) and
//! prints the time since this instant into its body.
//!
//! This two nodes can be put between one or several nodes. You can measure the time anything in between this nodes took and therefore identify
//! possible slow setups.

use dagxecutor::{Aggregator, Node};
use crate::nodes::NodeCategory;
use crate::{nodes::{NodeCreator, DagnNode, DefaultCreator, UiNode}, abst::audio::Clock};
use std::{sync::{Arc, RwLock}, time::Instant};
use neith::prelude::MutVal;

///The actual tick node, has no interface atm.
pub struct TimerTick;

impl Node for TimerTick{
    type Inputs = [Clock; 1];
    type Outputs = (Clock, Instant);
    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let [inner] = input;
	(inner, Instant::now())
    }

    fn debug_info(&self) -> &str {
	"TimerTick"
    }
}

impl DagnNode for TimerTick{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	Box::new(DefaultCreator::new(|executor|{

	    let node = TimerTick;
	    
	    Some(UiNode{
		node: Aggregator::from_node(node),
		interface: None,
		title: Some(String::from(TimerTick.debug_info())),
		category: NodeCategory::Debug,
		in_port_names: vec!["Clock"],
		out_port_names: vec!["Clock", "Timer Start"]
	    })
	}))
    }

    fn tags() -> Vec<String> {
	vec![
	    "Timer".to_string(),
	    "Performance".to_string(),
	    "Time".to_string(),
	    "measure".to_string(),
	]
    }

    fn name() -> String {
	"TimerTick".to_string()
    }
}

///The registering node. Prints the time of the connected Tick node into its interface and writes it as info into
/// the logs.
pub struct TimerReg{
    inter_string: MutVal<String>,
}

impl TimerReg{
    pub fn new() -> Self{
	TimerReg{
	    inter_string: MutVal::new(String::from("Nothing send yet!")),
	}
    }
}

impl Node for TimerReg{
    type Inputs = (Clock, Instant);
    type Outputs = [Clock; 1];
    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let dur = input.1.elapsed();
	*self.inter_string.get_mut() = format!("Last Time: {}sec / {}ms / {}ns", dur.as_secs(), dur.as_millis(), dur.as_nanos());

	[input.0]
    }

    fn debug_info(&self) -> &str {
	"TimerReg"
    }
}

impl DagnNode for TimerReg{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	Box::new(DefaultCreator::new(|executor|{

	    let node = TimerReg::new();
	    
	    
	    let inter = Arc::new(RwLock::new(
		neith::io::Edit::new(
		    neith::base::Text::new()
			.with_size(15.0)
		).with_mut_val(
		    node.inter_string.clone()
		)
	    ));

	    let show_name = node.debug_info().to_string();
	    Some(UiNode{
		node: Aggregator::from_node(node),
		interface: Some(inter),
		title: Some(String::from(show_name)),
		category: NodeCategory::Debug,
		in_port_names: vec!["Clock", "Timer Start"],
		out_port_names: vec!["Clock"]
	    })
	}))
    }

    fn name() -> String {
	"TimerReg".to_string()
    }

    fn tags() -> Vec<String> {
	vec![
	    "Timer".to_string(),
	    "Performance".to_string(),
	    "Time".to_string(),
	    "measure".to_string(),
	]
    }
}



