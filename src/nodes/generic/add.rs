use std::{ops::Add, marker::PhantomData, sync::{RwLock, Arc}};
use dagxecutor::{Executor, Node, AbstAggregator, Aggregator};
use neith::{prelude::Widget, io::DropDown};
use crate::nodes::NodeCategory;
use crate::{nodes::{NodeCreator, DagnNode, UiNode}, abst::audio::AudioBuffer};

pub struct Addition<A, B> where A: Add<B> + Add<Output = A> + Add<B, Output = A>+ Send + 'static, B: Send + 'static{
    ty1: PhantomData<A>,
    ty2: PhantomData<B>
}
impl<A, B> Addition<A, B> where A: Add<B> + Add<Output = A> + Add<B, Output = A>+ Send + 'static, B: Send + 'static{
    pub fn new() -> Self{
	Self{
	    ty1: PhantomData,
	    ty2: PhantomData
	}
    }
}

impl<A, B> Node for Addition<A, B> where A: Add<B> + Add<Output = A> + Add<B, Output = A>+ Send + 'static, B: Send + 'static{
    type Inputs = (A, B);
    type Outputs = [A; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let (a, b) = input;
	let res = a + b;
	[res]
    }
}


impl<A, B> DagnNode for Addition<A, B> where A: Add<B> + Add<Output = A> + Add<B, Output = A>+ Send + 'static, B: Send + 'static{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	Box::new(AdditionCreator{
	    type_chooser: Arc::new(RwLock::new(
		DropDown::new()
		    .with_elements(
			vec![
			    "AudioBuffer".to_string(), 
			    "f32".to_string(),
			    "f64".to_string(),
			    "usize".to_string(),
			    "isize".to_string(),
			    "f32 to AudioBuffer".to_string(),
			    "f64 to AudioBuffer".to_string(),
			]
		    ).with_selection(0)
	    ))
	})
    }

    fn tags() -> Vec<String> {
	vec!["Math".to_string(), "add".to_string(), "generic".to_string()]
    }

    fn name() -> String {
	"Addition".to_string()
    }
}



struct AdditionCreator{
    type_chooser: Arc<RwLock<DropDown<String>>>
}

impl NodeCreator for AdditionCreator{
    fn create_node(&self, executor: Arc<Executor>) -> Option<UiNode>{
	let node: Arc<RwLock<dyn AbstAggregator + Send + Sync>> = match self.type_chooser.read().unwrap().get_selected().unwrap().as_str(){
	    "AudioBuffer" => Aggregator::from_node(Addition::<AudioBuffer, AudioBuffer>::new()),
	    "f32" => Aggregator::from_node(Addition::<f32, f32>::new()),
	    "f64" => Aggregator::from_node(Addition::<f64, f64>::new()),
	    "usize" => Aggregator::from_node(Addition::<usize, usize>::new()),
	    "isize" => Aggregator::from_node(Addition::<isize, isize>::new()),
	    "f32 to AudioBuffer" => Aggregator::from_node(Addition::<AudioBuffer, f32>::new()),
	    "f64 to AudioBuffer" => Aggregator::from_node(Addition::<AudioBuffer, f64>::new()),
	    _ => return None,
	};
	
	Some(UiNode{
	    node,
	    interface: None,
	    title: Some(String::from("Addition")),
	    category: NodeCategory::ScalarMath,
	    in_port_names: vec!["Value"],
	    out_port_names: vec!["Value 1", "Value 2"]
	})
    }
    
    fn creator_interface(&self) -> Arc<RwLock<dyn Widget + Send + Sync>>{
	self.type_chooser.clone()
    }  
}
