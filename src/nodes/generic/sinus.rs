
use dagxecutor::{Aggregator, Node, Executor};
use crate::nodes::NodeCategory;
use crate::{nodes::{NodeCreator, DagnNode, DefaultCreator, UiNode}, abst::audio::{AudioBuffer, Clock}};
use neith::prelude::{Widget, MutVal};
use std::sync::{RwLock, Arc, Mutex};

pub struct Sinus{
    current_value: MutVal<String>,
}

impl Sinus{
    pub fn new() -> Self{
	Sinus{
	    current_value: MutVal::new(String::from("No Buffer Yet!")),
	}
    }
}

impl Node for Sinus{
    //Clock and frequency
    type Inputs = [f64; 1];
    //Outputs Clock with sinus in buffer as well as the last value that was set.
    type Outputs = [f64; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	*self.current_value.get_mut() = format!("Sin(In)={}", input[0].sin());
	[input[0].sin()]
    }
}



impl DagnNode for Sinus{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	Box::new(DefaultCreator::new(|executor|{

	    let node = Sinus::new();
	    
	    let interface = Arc::new(RwLock::new(
		neith::io::Edit::new(neith::base::Text::new())
		    .with_mut_val(node.current_value.clone())
	    ));

	    Some(UiNode{
		node: Aggregator::from_node(node),
		interface: Some(interface),
		title: Some(String::from("Sinus")),
		category: NodeCategory::ScalarMath,
		in_port_names: vec!["InValue"],
		out_port_names: vec!["Sin(InValue)"]
	    })
	}))
    }

    fn tags() -> Vec<String> {
	vec![
	    "Value".to_string(),
	    "Math".to_string(),
	    "Wave".to_string(),
	    "Sinus".to_string(),
	]
    }

    fn name() -> String {
	"Sinus".to_string()
    }
}

struct SinusCreator{
    input_ty: MutVal<String>,
    interface: Arc<RwLock<dyn Widget + Send + Sync>>
}
