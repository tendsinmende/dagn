use dagxecutor::Aggregator;
use dagxecutor::Executor;
use dagxecutor::Node;
use dagxecutor::AbstAggregator;
use neith::prelude::MutVal;
use neith::prelude::Widget;

use crate::abst::misc::IntoNormValue;
use crate::nodes::DagnNode;
use crate::nodes::NodeCategory;
use crate::nodes::NodeCreator;
use crate::nodes::UiNode;
use crate::abst::misc::NormVal;
use std::sync::{Arc, RwLock};

pub struct ToNormValue<T>{
    min: MutVal<T>,
    max: MutVal<T>
}

impl<T: IntoNormValue + Send + Sync + Clone + 'static> Node for ToNormValue<T>{
    //Clock and frequency
    type Inputs = [T; 1];
    //Outputs Clock with sinus in buffer as well as the last value that was set.
    type Outputs = [NormVal; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let [val] = input;
	[val.into_norm(self.min.get().clone(), self.max.get().clone())]
    }
}



impl<T: IntoNormValue + Send + Sync + Clone + 'static> DagnNode for ToNormValue<T>{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	let input_ty_dd = Arc::new(RwLock::new(
	    neith::io::DropDown::new()
		.with_elements(vec![
		    "f32".to_string(),
		    "f64".to_string(),
		    "usize".to_string(),
		    "isize".to_string(),
		    "u8".to_string(),
		    "u16".to_string(),
		    "u32".to_string(),
		    "u64".to_string(),
		    "u128".to_string(),
		]).with_selection(0)
	));
	
	let interface = Arc::new(RwLock::new(
	    neith::layout::Split::new()
		.with_one(neith::io::Label::new("Input type to be normalized"))
		.with_two_arc(
		    input_ty_dd.clone()
		)
	));
	Box::new(ToNormValCreator{
	    interface,
	    input_ty: input_ty_dd
	})	
    }

    fn tags() -> Vec<String> {
	vec![
	    "Value".to_string(),
	    "Math".to_string(),
	    "Wave".to_string(),
	    "Sinus".to_string(),
	]
    }

    fn name() -> String {
	"ToNormalize".to_string()
    }
}

struct ToNormValCreator{
    input_ty: Arc<RwLock<neith::io::DropDown<String>>>,
    interface: Arc<RwLock<dyn Widget + Send + Sync>>
}

impl NodeCreator for ToNormValCreator{
    fn create_node(&self, _executor: Arc<Executor>) -> Option<UiNode> {

	let min_split = Arc::new(RwLock::new(
	    neith::layout::Split::new()
		.with_one(neith::io::Label::new("Min"))
	));

	let max_split = Arc::new(RwLock::new(
	    neith::layout::Split::new()
		.with_one(neith::io::Label::new("Min"))
	));
	
	let interface = Arc::new(RwLock::new(
	    neith::layout::Split::new()
		.with_one_arc(
		    min_split.clone()
		).with_two_arc(
		    max_split.clone()		    
		)
	));

	let aggregator: Arc<RwLock<dyn AbstAggregator + Send + Sync>> = match self.input_ty.read().unwrap().get_selected().unwrap().as_str(){
	    "f32" => {
		let node = ToNormValue{
		    min: MutVal::new(0.0 as f32),
		    max: MutVal::new(1.0 as f32)
		};
		min_split.write().unwrap().set_two(
		    neith::io::ValEdit::new(1.0 as f32)
			.with_mut_val(node.min.clone())
		);  
		max_split.write().unwrap().set_two(
		    neith::io::ValEdit::new(1.0 as f32)
			.with_mut_val(node.max.clone())
		);
		Aggregator::from_node(node)
	    },
	    "f64" => {
		let node = ToNormValue{
		    min: MutVal::new(0.0 as f64),
		    max: MutVal::new(1.0 as f64)
		};
		min_split.write().unwrap().set_two(
		    neith::io::ValEdit::new(1.0 as f64)
			.with_mut_val(node.min.clone())
		);  
		max_split.write().unwrap().set_two(
		    neith::io::ValEdit::new(1.0 as f64)
			.with_mut_val(node.max.clone())
		);
		Aggregator::from_node(node)
	    },
	    
	    _ => return None,
	};

	
	
	Some(UiNode{
	    node: aggregator,
	    interface: Some(interface),
	    title: Some(String::from("ToNormValue")),
	    category: NodeCategory::ScalarMath,
	    in_port_names: vec!["Value"],
	    out_port_names: vec!["Normalized Value"],
	    
	})
    }

    fn creator_interface(&self) -> Arc<RwLock<dyn Widget + Send + Sync>> {
	self.interface.clone()
    }
}
