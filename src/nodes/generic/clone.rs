//! Clones some value. Use the macro to implement this for your own type. Don't forget to add it to
//! add it to the register table when creating a new one tho.

use std::sync::{Arc, Mutex};

use dager::{Aggregator, Executor, Node};
use crate::{abst::audio::AudioBuffer, nodes::{IntoUiNode, NodeCategory, PortInfo, UiNode}};


///Implements a node called `name` that will produce a slider set value of type `implty` every time the clock is active.
macro_rules! impl_CloneValue{
    ($name:ident, $implty:ty) => {
	pub struct $name;

	impl Node for $name{
	    type InSig = [$implty; 1];
	    type OutSig = ($implty, $implty);
	    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
		let [i] = input;
		(i.clone(), i)
	    }
	}


	impl IntoUiNode for $name{
	    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>{

		let node = $name;
		let ui_generator = crate::nodes::void_generator();

		let aggregator = Aggregator::from_node(node);
		let mut in_ports = PortInfo::from_nodes_in(&aggregator);
		in_ports[0].name = "Value".to_string();
		let mut out_ports = PortInfo::from_nodes_out(&aggregator);
		out_ports[0].name = "Cloned".to_string();
		out_ports[1].name = "Value".to_string();
		Some(UiNode{
		    node: Arc::new(Mutex::new(aggregator)),
		    interface: ui_generator,
		    title: String::from(stringify!($name)),
		    category: NodeCategory::None,
		    in_ports,
		    out_ports,
		    executor
		})
	    }
	}
    }
}


impl_CloneValue!(CloneAudioBuffer, AudioBuffer);
impl_CloneValue!(Clonef32, f32);
impl_CloneValue!(Clonef64, f64);
impl_CloneValue!(Cloneu32, u32);
impl_CloneValue!(Cloneu64, u64);
impl_CloneValue!(Clonei32, i32);
impl_CloneValue!(Clonei64, i64);
