
/*
///Adds two dates of the same type
pub mod add;
pub use add::*;

///Multiplies two dates of the same type
pub mod multiply;
pub use multiply::*;

///Contrary to the simpl_sinus, this node just calculates the sinus of the input value
pub mod sinus;
pub use sinus::*;

///Simple Timer nodes that can be used to measure time needed in between nodes.
pub mod timer;
pub use timer::*;


///Converts a value into a NormVal, based on min and max.
pub mod to_normval;
pub use to_normval::*;
*/

///Takes some value and clones it. NOTE: Depending on the data this operation might be costly.
pub mod clone;
pub use clone::*;

