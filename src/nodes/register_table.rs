use crate::interface::node_canvas::CreatorFunc;
use crate::interface::node_canvas::CreatorStore;

use super::{NodeCategory, IntoUiNode, audio::*, generic::*};

fn get_register_table() -> Vec<(String, NodeCategory, CreatorFunc)>{
    vec![
	//Sound related
	("Mix".to_string(), NodeCategory::Audio, Box::new(|ex| Mix::into_ui_node(ex))),
	("Clock Tick".to_string(), NodeCategory::Audio, Box::new(|ex| ClockTick::into_ui_node(ex))),
	("Cpal Producer".to_string(), NodeCategory::Audio, Box::new(|ex| CpalProducer::into_ui_node(ex))),
	("Cpal Consumer".to_string(), NodeCategory::Audio, Box::new(|ex| CpalConsumer::into_ui_node(ex))),
	("Simple Sinus".to_string(), NodeCategory::Audio, Box::new(|ex| SimpleSinus::into_ui_node(ex))),
	("FM-Synth".to_string(), NodeCategory::Audio, Box::new(|ex| FMSynth::into_ui_node(ex))),
	("ClockToValue".to_string(), NodeCategory::Audio, Box::new(|ex| ClockToVal::into_ui_node(ex))),
	("Monos to Stereo".to_string(), NodeCategory::Audio, Box::new(|ex| MonosToStereo::into_ui_node(ex))),
	("To Panned Stereo".to_string(), NodeCategory::Audio, Box::new(|ex| PannedStereo::into_ui_node(ex))),
	("Wave Editor".to_string(), NodeCategory::Audio, Box::new(|ex| WaveEditor::into_ui_node(ex))),
	
	("Clocked f32".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedf32::into_ui_node(ex))),
	("Clocked f64".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedf64::into_ui_node(ex))),
	("Clocked u32".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedu32::into_ui_node(ex))),
	("Clocked u64".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedu64::into_ui_node(ex))),
	("Clocked i32".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedi32::into_ui_node(ex))),
	("Clocked u64".to_string(), NodeCategory::Audio, Box::new(|ex| Clockedi64::into_ui_node(ex))),

	//Clone implementations
	("Clone AudioBuffer".to_string(), NodeCategory::Audio, Box::new(|ex| CloneAudioBuffer::into_ui_node(ex))),
	("Clone f32".to_string(), NodeCategory::None, Box::new(|ex| Clonef32::into_ui_node(ex))),
	("Clone f64".to_string(), NodeCategory::None, Box::new(|ex| Clonef64::into_ui_node(ex))),
	("Clone u32".to_string(), NodeCategory::None, Box::new(|ex| Cloneu32::into_ui_node(ex))),
	("Clone u64".to_string(), NodeCategory::None, Box::new(|ex| Cloneu64::into_ui_node(ex))),
	("Clone i32".to_string(), NodeCategory::None, Box::new(|ex| Clonei32::into_ui_node(ex))),
	("Clone i64".to_string(), NodeCategory::None, Box::new(|ex| Clonei64::into_ui_node(ex))),

    ]
}

///Used to register all nodes specified in the vector of the `get_register_table()` function.
pub fn register_table(store: &mut CreatorStore){
    for (name, category, creator) in get_register_table().into_iter(){
	store.add(name, category, creator);
    }
}
