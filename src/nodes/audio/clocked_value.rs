//! A ClockedValue is produced every time the clock input is some how triggered.

use std::sync::{Arc, Mutex};

use dager::{Aggregator, Executor, Node};
use neith::{com::MutVal, io::{Label, Slider}, widget::Widget};

use crate::{abst::audio::Clock, nodes::{IntoUiNode, NodeCategory, PortInfo, UiNode}};


///Implements a node called `name` that will produce a slider set value of type `implty` every time the clock is active.
macro_rules! impl_ClockedValue{
    ($name:ident, $implty:ty) => {
	pub struct $name{
	    value: MutVal<$implty>,
	}

	impl $name{
	    pub fn new(val: $implty) -> Self{
		$name{
		    value: MutVal::new(val),
		}
	    }
	}

	impl Node for $name{
	    type InSig = [Clock; 1];
	    type OutSig = ($implty, Clock);
	    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
		let [clock] = input;
		(self.value.get().clone(), clock)
	    }
	}


	impl IntoUiNode for $name{
	    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>{

		let node = $name{value: MutVal::new(1 as $implty)};
		let valuecp = node.value.clone();
		
		let ui_generator = Box::new(move |_ex| {
		    let valuecp = valuecp.clone();
		    let widget: Box<dyn Widget + Send + 'static> = Box::new(
			Slider::new(1 as $implty, 0 as $implty, 1000 as $implty, 1 as $implty)
			    .with_mut_val(valuecp)
			    .with_label(Label::new(""), None)
		    );
		    widget
		});

		let aggregator = Aggregator::from_node(node);
		let mut in_ports = PortInfo::from_nodes_in(&aggregator);
		in_ports[0].name = "Clock".to_string();
		let mut out_ports = PortInfo::from_nodes_out(&aggregator);
		out_ports[0].name = "Value".to_string();
		out_ports[1].name = "Clock".to_string();
		Some(UiNode{
		    node: Arc::new(Mutex::new(aggregator)),
		    interface: ui_generator,
		    title: String::from(stringify!($name)),
		    category: NodeCategory::Audio,
		    in_ports,
		    out_ports,
		    executor
		})
	    }
	}
    }
}


impl_ClockedValue!(Clockedf32, f32);
impl_ClockedValue!(Clockedf64, f64);
impl_ClockedValue!(Clockedu32, u32);
impl_ClockedValue!(Clockedu64, u64);
impl_ClockedValue!(Clockedi32, i32);
impl_ClockedValue!(Clockedi64, i64);

