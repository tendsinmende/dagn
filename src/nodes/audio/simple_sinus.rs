
use std::sync::{RwLock, Arc, Mutex};

use dager::AbstAggregator;
use dager::Aggregator;
use dager::Executor;
use dager::Node;
use neith::{com::MutVal, io::Slider};
use neith::io::TextEditable;
use neith::widget::Widget;

use crate::abst::audio::AudioBuffer;
use crate::nodes::IntoUiNode;
use crate::nodes::NodeCategory;
use crate::nodes::PortInfo;
use crate::nodes::UiNode;

pub struct SimpleSinus{
    current_value: MutVal<String>,
    default_freq: MutVal<f64>,
    phase: f64,
}

impl SimpleSinus{
    pub fn new() -> Self{
	SimpleSinus{
	    current_value: MutVal::new(String::from("No Buffer Yet!")),
	    default_freq: MutVal::new(440.0),
	    phase: 0.0,
	}
    }
}

impl Node for SimpleSinus{
    //Clock and frequency
    type InSig = (AudioBuffer, f64);
    //Outputs Clock with sinus in buffer as well as the last value that was set.
    type OutSig = (AudioBuffer, f64);

    ///Outputs sinus in Clocks's buffer, and the last value on the f64 that was calculated. However, both are only send when the clocks value is
    /// Clock::NewFrame. Otherwise the clocksignal and 0.0 are send.
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (mut buffer, frequency) = input;
	let sr = buffer.sample_rate() as f64;
	//Per sample, advance phase and push inside the sin value
	for sample in buffer.buffer.iter_mut(){
	    self.phase += frequency * 2.0 * std::f64::consts::PI / sr;
	    
	    *sample = self.phase.sin() as f32;
	}
	
	let last_val = buffer.buffer[buffer.buffer.len() - 1] as f64; //Get last value and send as well
	*self.current_value.get_mut() = format!("Last sample: {}", last_val);
	(buffer, last_val)
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator) {
	aggregator.set_default_value(1, Box::new(*self.default_freq.get()));
    }
}



impl IntoUiNode for SimpleSinus{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let node = SimpleSinus::new();

	let node_mut_val = node.current_value.clone(); 
	let node_default_value = node.default_freq.clone();
	
	let interface_generator = Box::new(move |_ex|{
	    let node_mut_val = node_mut_val.clone();
	    Box::new(
		neith::io::Label::new("Sinus value")
		    .with_mut_val(node_mut_val)
	    ) as Box<dyn Widget + Send + 'static>
	});

	let node = Arc::new(Mutex::new(Aggregator::from_node(node)));
	let mut in_ports = PortInfo::from_nodes_in(&*node.lock().unwrap());
	in_ports[0].name = "AudioBuffer".to_string();
	in_ports[1].name = "Frequency".to_string();
	in_ports[1].shared_default_value = Some(Box::new(Slider::new(1.0, 1.0, 28_000.0, 1.0).with_mut_val(node_default_value)));
	let mut out_ports = PortInfo::from_nodes_in(&*node.lock().unwrap());
	out_ports[0].name = "AudioBuffer".to_string();
	out_ports[1].name = "LastSample".to_string();
	
	Some(UiNode{
	    node,
	    interface: interface_generator,
	    title: String::from("Simple Sinus"),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}

