use cpal::{self, SampleRate, SupportedStreamConfig};
use cpal::Data;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use crossbeam_channel::{bounded, Sender};
use dager::AbstAggregator;
use dager::Executable;
use dager::Aggregator;
use dager::Executor;
use dager::Node;
use neith::com::MutVal;
use neith::config::Color;
use neith::widget::Widget;
use neith_widgets::graph::GraphHost;
use crate::{abst::audio::{Channel, Sample, StereoChannel}, nodes::void_generator};
use crate::abst::audio::Time;
use crate::nodes::IntoUiNode;
use crate::nodes::NodeCategory;
use crate::nodes::PortInfo;
use crate::{nodes::UiNode, abst::audio::{Clock, AudioBuffer, AudioState}};
use log::{error, info, trace, warn};
use std::{time::Duration, sync::{RwLock, Arc, Mutex}, thread::JoinHandle};
use neith::layout::Direction;

type CpalAudioChannel = Sender<StereoChannel>;



///Creates a cpal device and output stream config based on the current config. Returns an error describing the problem if something went wrong.
fn get_cpal_output() -> Result<(cpal::Device, SupportedStreamConfig), String>{

    let target_conf = crate::config::DagnConfig::get().cpal_config;

    //Try to find a device with the given name
    let device = if let Some(found_device) = cpal::default_host().devices().unwrap().fold(None, |acc, dev|{
	if let Some(d) = acc{
	    Some(d)
	}else{
	    if dev.name().unwrap() == target_conf.out_service_name{
		trace!("Found device with name {}", target_conf.out_service_name);
		Some(dev)
	    }else{
		None
	    }
	}
    }){
	found_device
    }else{
	error!("Could not find device service with name {}", target_conf.out_service_name);
	return Err("Did not find device".to_string());
    };

    //Search for all configs that support enough channels
    let config_with_channels = device.supported_output_configs().unwrap().into_iter().fold(Vec::new(), |mut supported_configs, config|{
	if config.channels() == target_conf.out_target_channels as u16{
	    supported_configs.push(config)
	}

	supported_configs
    });

    if config_with_channels.len() == 0{
	error!("Could not find config with {} channels on device {}", target_conf.out_target_channels, device.name().unwrap());
	return Err("Could not find config with the set amount of channels".to_string());
    }

    //Since we have a range of configs now, check if any of those configs has our desired sampling range.
    let mut final_config = None;
    for c in config_with_channels{
	if c.min_sample_rate().0 <= target_conf.out_target_sampling as u32 && c.max_sample_rate().0 >= target_conf.out_target_sampling as u32{
	    final_config = Some(c.with_sample_rate(SampleRate(target_conf.out_target_sampling as u32)));
	    break;
	}
    }

    if let Some(fc) = final_config{
	trace!("Create device {} with config: sampling={}, channel={}", device.name().unwrap(), target_conf.out_target_sampling, target_conf.out_target_channels);
	Ok((device, fc))
    }else{
	error!(
	    "Could not setup config with {} channels on device {} with correct sampling {}.",
	    target_conf.out_target_channels, device.name().unwrap(), target_conf.out_target_sampling
	);
	Err("Could not setup any config range with correct sampling".to_string())
    }
}


pub struct CpalProducer{
    //Keeps the cpal device alive until we exit this node
    cpal_device: cpal::Device,

    stream_handle: Option<JoinHandle<()>>,

    end_state: Arc<RwLock<bool>>,
    ///Some if the audio thread prepared a audio buffer.
    next_buffer: Option<(CpalAudioChannel, AudioBuffer)>,

    schedule_state_change: MutVal<Option<AudioState>>,
    
    state: MutVal<AudioState>
}

type CpalProdOutSig = (AudioBuffer, Clock, CpalAudioChannel);
impl Node for CpalProducer{
    type InSig = ();
    type OutSig = CpalProdOutSig;

    fn process(&mut self, _input: Self::InSig) -> Self::OutSig {
	//Create generate a clock value if there is a state_change pending.
	// if we are in streaming mode, create a audio buffer and send that.

	//Check if there is a state change happening
	let state_change_to_send = if let Some(change) = self.schedule_state_change.get_mut().take(){
	    *self.state.get_mut() = change;
	    self.state.get().into_clock()
	}else{
	    //Check current state and act if streaming
	    match *self.state.get(){
		AudioState::Playing => {
		    if let Some((channel, ab)) = self.next_buffer.take(){
			info!("Sending New fram with buffer");
			return (ab, Clock::NewFrame, channel);
		    }else{
			warn!("No audio buffer was prepared!");
			Clock::NewFrame
		    }
		},
		_ => {
		    warn!("Sending Stop or Pause signal, however, thread should wait...");
		    Clock::Stop
		}
	    }
	};
	let (dummy_sender, _dummy_receiver) = bounded::<StereoChannel>(2);
	(AudioBuffer::new(1,1), state_change_to_send, dummy_sender)
    }
}

///Dropping Producer, therefore send end to thread and wait for it
impl Drop for CpalProducer{
    fn drop(&mut self){
	*self.end_state.write().unwrap() = true;
	if let Some(handle) = self.stream_handle.take(){
	    if let Err(e) = handle.join(){
		error!("Failed to join cpal thread: {:?}", e);
	    }
	}
    }
}

impl IntoUiNode for CpalProducer{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>{
	//The cpal producer tries to setup a device and a stream to this device.
	// Then a streaming function is build that is called by the audio driver everytime it needs a new
	// input.

	let (device, config) = match get_cpal_output(){
	    Ok(devconf) => devconf,
	    Err(e) => {
		error!("Failed to create CpalDevice: {}", e);
		return None;
	    }
	};
	

	
	info!(
	    "CpalConfig: sample_rate={:?}, num_channel={:?}, sample_type={:?}",
	    config.sample_rate(),
	    config.channels(),
	    config.sample_format()
	);

	assert!(config.channels() == 2, "Assuming a stereo system, however head != 2 channels!");
	
	let node = Arc::new(Mutex::new(CpalProducer{
	    cpal_device: cpal::Device::from(device),
	    stream_handle: None,
	    end_state: Arc::new(RwLock::new(false)),
	    next_buffer: None,
	    schedule_state_change: MutVal::new(None),
	    state: MutVal::new(AudioState::Stopped),
	}));

	let async_node = node.clone();
	//Changed by the ui to a value if a state change should be done
	let to_state = MutVal::new(None);
	let end_state = node.lock().unwrap().end_state.clone();

	//Create aggregator thats being used
	let node_agg = Arc::new(Mutex::new(Aggregator::from_arc(node.clone())));


	//depending on the sample type, start a stream that sends out a new frame, every time the
	// the driver "wants" it.
	let stream_handle = match config.sample_format(){
	    cpal::SampleFormat::F32 => audio_callback::<f32>(async_node, node_agg.clone(), to_state.clone(), end_state, config.into(), executor.clone()),
	    cpal::SampleFormat::I16 => audio_callback::<i16>(async_node, node_agg.clone(), to_state.clone(), end_state, config.into(), executor.clone()),
	    cpal::SampleFormat::U16 => audio_callback::<u16>(async_node, node_agg.clone(), to_state.clone(), end_state, config.into(), executor.clone()),
	};
	
	node.lock().unwrap().stream_handle = Some(stream_handle);

	let ui_generator = Box::new(move |_ex: Arc<Executor>|{
	    //Generate ui
	    let sc1 = to_state.clone();
	    let sc2 = to_state.clone();
	    let sc3 = to_state.clone();

	    let ui: Box<dyn Widget + Send + 'static> = Box::new(neith::layout::StaticList::new(Direction::Vertical)
		.with_items(vec![
		    Box::new(
			neith::io::Button::new("Play")
			    .with_action(move || *sc1.get_mut() = Some(AudioState::Playing))
		    ),
		    Box::new(
			neith::io::Button::new("Pause")
			    .with_action(move || *sc2.get_mut() = Some(AudioState::Paused))
		    ),
		    Box::new(
			neith::io::Button::new("Stop")
			    .with_action(move || *sc3.get_mut() = Some(AudioState::Stopped))
		    )
		]));

	    ui
	});
	
	//Now setup ui node used
	let in_ports = PortInfo::from_nodes_in(&*node_agg.lock().unwrap());
	let mut out_ports = PortInfo::from_nodes_out(&*node_agg.lock().unwrap());
	out_ports[0].name = "Audiobuffer".to_string();
	out_ports[1].name = "Clock".to_string();
	out_ports[2].name = "CpalCallback (must connect)".to_string();
	
	Some(UiNode{
	    node: node_agg,
	    interface: ui_generator,
	    title: "Cpal Out".to_string(),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}

//Builds the callback function depending on the sample format `T` that was chosen
fn audio_callback<T: cpal::Sample>(
    parent_node: Arc<Mutex<CpalProducer>>,
    parent_aggregator: Arc<Mutex<Aggregator<(), CpalProdOutSig>>>,
    change_to_state: MutVal<Option<AudioState>>,
    end_state: Arc<RwLock<bool>>,
    config: cpal::StreamConfig,
    executor: Arc<Executor>
) -> JoinHandle<()>{

    let sample_rate = config.sample_rate.0 as usize;
    let num_channel = config.channels as usize;

    //Since the stream is not send able, we create out own low frequent thread, that only manages pause/play of this stream.
    //the join handle is used to stop the thread when needed (at dropping).
    // this is not nice, but needed mostly because the windows stream handle is not Send.
    let stream_handle = std::thread::spawn(move||{

	let node_cp = parent_node.clone();
	let inner_node_cp = node_cp.clone();
	let exec_cp = executor.clone();
	let inner_exec_cp = exec_cp.clone();
	//Create the stream callback
	let stream = node_cp.lock().unwrap().cpal_device.build_output_stream(
	    &config,
	    move |data: &mut [T], callback_info: &cpal::OutputCallbackInfo|{
		//TODO at some point use the callback time stamp?
		let inner_exec_cp = inner_exec_cp.clone();
		let inner_node_cp = inner_node_cp.clone();
		
		//Depending on the channels, generate one audio buffer (for both channels). The user is free to split this
		// channel at some point into various buffers
		if data.len() % num_channel != 0{
		    error!("Cpal: Data slice is not dividable by the channel count: {} % {} = {}", data.len(), num_channel, data.len() % num_channel);
		}
		
		//Now setup the audio buffer and the channel to send back, pack them into the node and execute
		let (sender, receiver) = bounded::<StereoChannel>(100);
		//Create initial audio buffer, 
		let sample_per_channel = data.len() / num_channel;		
		let audio_buffer = AudioBuffer::new(sample_per_channel, sample_rate);

		inner_node_cp.lock().unwrap().next_buffer = Some((sender, audio_buffer));
		if let Err(e) = parent_aggregator.lock().unwrap().execute(inner_exec_cp){
		    warn!("Failed to execute cpal producer in stream: {:?}", e);
		}

		//Wait some time for the graph to finish.
		let wait_time = Duration::from_secs_f64((1.0 / sample_rate as f64) * sample_per_channel as f64);
		//Now wait for each receiver. Then fill the buffer with output
		if let Ok(filled_buffer) = receiver.recv_timeout(wait_time){
		    let audio_buffers = filled_buffer.dissolve();

		    //Currently assuming that we are on a stereo system, otherwise well paniced before.
		    let mut sample_idx = 0;
		    for frame in data.chunks_mut(num_channel){
			
			let left_sample = cpal::Sample::from::<f32>(&audio_buffers[0].samples().get(sample_idx).unwrap_or(&0.0));
			let right_sample = cpal::Sample::from::<f32>(&audio_buffers[1].samples().get(sample_idx).unwrap_or(&0.0));

			frame[0] = left_sample;
			frame[1] = right_sample;
			
			sample_idx += 1;
		    }
		}else{
		    //receiving failed, fill with "nothing"
		    error!("Cpal could not receive filled audio buffer!");
		    for s in data.iter_mut(){
			*s = cpal::Sample::from::<f32>(&0.0);
		    }
		}		
	    },
	    |error: cpal::StreamError|{
		error!("Cpal stream error: {}", error);
	    }
	);

	let stream = match stream{
	    Ok(s) => s,
	    Err(e) => {
		error!("Failed to start nodes Cpal stream: {}", e);
		return;
	    },
	};

	let _ = stream.pause();

	//Keep the stream alive until we receive the end signal
	while !*end_state.read().unwrap(){
	    std::thread::sleep(std::time::Duration::from_millis(100));
	    //Now, go into a loop that checks the "go to" state of this node. If a state change should be done,
	    // change the streams state, then notify the node which, in turn will notify the graph.
	    if let Some(new_state) = change_to_state.get_mut().take(){
		match new_state{
		    AudioState::Paused => {
			if let Err(e) = stream.pause(){
			    error!("Failed to pause Cpal stream: {}", e);
			}
			*node_cp.lock().unwrap().schedule_state_change.get_mut() = Some(AudioState::Paused);
		    },
		    AudioState::Playing => {
			if let Err(e) = stream.play(){
			    println!("Failed to play cpal stream: {}", e)
			}
			*node_cp.lock().unwrap().schedule_state_change.get_mut() = Some(AudioState::Playing);
		    },
		    AudioState::Stopped => {
			if let Err(e) = stream.pause(){
			    println!("Failed to pause cpal stream: {}", e)
			}
			*node_cp.lock().unwrap().schedule_state_change.get_mut() = Some(AudioState::Stopped);
		    },
		}
	    }
	    //Give back to scheduler
	    std::thread::yield_now(); 
	}
	
    });
    
    stream_handle
}

pub struct CpalConsumer;

impl Node for CpalConsumer{
    type InSig = (StereoChannel, CpalAudioChannel);
    type OutSig = ();

    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (stereo_sig, send_channel) = input;

	//Finally send buffer back
	if let Err(e) = send_channel.send(stereo_sig){
	    error!("Cpal consumer failed to send audio buffer: {}", e);
	};
    }
}

impl IntoUiNode for CpalConsumer{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let node = CpalConsumer;

	let node = Arc::new(Mutex::new(Aggregator::from_node(node)));
	let mut in_ports = PortInfo::from_nodes_in(&*node.lock().unwrap());
	in_ports[0].name = "Stereo Signal".to_string();
	in_ports[1].name = "Callback (must connect)".to_string();
	let out_ports = PortInfo::from_nodes_out(&*node.lock().unwrap());
	
	Some(UiNode{
	    node,
	    interface: void_generator(),
	    title: String::from("CpalConsumer"),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
