use std::sync::Arc;
use std::sync::Mutex;

use dager::AbstAggregator;
use dager::Aggregator;
use dager::Executor;
use dager::Executable;
use dager::Node;
use neith::com::MutVal;
use neith::io::Label;
use neith::layout::HorizontalAlign;
use neith::layout::VerticalAlign;
use neith::widget::Widget;
use crate::abst::audio::{AudioBuffer, Clock};
use crate::nodes::IntoUiNode;
use crate::nodes::NodeCategory;
use crate::nodes::PortInfo;
use crate::nodes::UiNode;

use log::warn;

///A node with an internal button, that when pressed fires a new Clock event. Good when something is not working and you are debugging a part of the graph.
pub struct ClockTick{
    buffer_size: MutVal<usize>,
    sample_rate: MutVal<usize>,
}

impl ClockTick{
    pub fn new() -> Self{
	ClockTick{
	    buffer_size: MutVal::new(1),
	    sample_rate: MutVal::new(44100),
	}
    }
}

impl Node for ClockTick{
    type InSig = ();
    type OutSig = (AudioBuffer, Clock);
    fn process(&mut self, input: Self::InSig) -> Self::OutSig{
	(AudioBuffer::new(*self.buffer_size.get(), *self.sample_rate.get()), Clock::NewFrame)
    }

    fn name<'a>(&'a self) -> &'a str {
	"Clock Tick"
    }
}

impl IntoUiNode for ClockTick{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>{
	let node = Arc::new(Mutex::new(ClockTick::new()));
	let node_aggregator = Arc::new(Mutex::new(Aggregator::from_arc(node.clone())));
	//Spawn thread that ticks the graph when the button is pressed

	let inner_node = node.clone();
	let agg_clone = node_aggregator.clone();

	println!("Create generator");

	
	let ui_generator = Box::new(move |ex: Arc<Executor>| {
	    let sample_rate = inner_node.lock().unwrap().sample_rate.clone();
	    let buffer_size = inner_node.lock().unwrap().buffer_size.clone();
	    let moved_agg = agg_clone.clone();
	
	    let excpy: Arc<Executor> = ex.clone();

	    let widget: Box<dyn Widget + Send + 'static> = Box::new(
		neith::layout::Split::new()
		    .with_one(
			neith::layout::Split::new()
			    .with_one(
				neith::io::Slider::<usize>::new(1, 1, 1024, 1)
				    .with_mut_val(buffer_size)
				    .with_label(
					Label::new("Buffer size"),
					Some(" sample [Buffer Size]")
				    )
			    )
			    .with_two(
				neith::io::Slider::<usize>::new(44100, 1, 96000, 1000)
				    .with_mut_val(sample_rate)
				    .with_label(
					Label::new("SampleRate"),
					Some(" Hz [Sample rate]")
				    )
			    )
		    ).with_two(
			neith::layout::Align::new(
			    neith::io::Button::new("Send Signal")
				.with_action(move||{
				    let excpy = excpy.clone();
				    //Send
				    moved_agg.lock().unwrap().execute(excpy).expect("Failed to exec tick");
				})
			).with_child_size((200.0, 75.0).into())
			    .with_vertical_alignment(VerticalAlign::Center)
			    .with_horizontal_alignment(HorizontalAlign::Center)
		    ));
	    println!("Created widget!");
	    widget
	});

	println!("Return generator");

	let node_aggregator: Arc<Mutex<dyn AbstAggregator + Send>> = node_aggregator;
	let in_ports = PortInfo::from_nodes_in(&*node_aggregator.lock().unwrap());
	let mut out_ports = PortInfo::from_nodes_out(&*node_aggregator.lock().unwrap());
	out_ports[0].name = "AudioBuffer".to_string();
	out_ports[1].name = "Clock".to_string();
	
	Some(UiNode{
	    node: node_aggregator,
	    interface: ui_generator,
	    title: String::from("ClockTick"),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor,
	})
    }
}
