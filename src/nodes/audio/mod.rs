

///Node with a button that send a new clock event into its children.
pub mod clock_tick;

pub use clock_tick::ClockTick;

/*
///A JackBridge produces a more sophisticated node, that can be setup with any amount of audio and midi outputs. The JackOut is the connection back to the
/// Jack-Server.
#[cfg(unix)]
pub mod jack_client;
#[cfg(unix)]
pub use jack_client::*;
*/

///Simple sinus that generates a sinus signal based on the set values.
pub mod simple_sinus;
pub use simple_sinus::*;


///Some value that can be set by hand and is set every time the clock comes through
pub mod clocked_value;
pub use clocked_value::*;

///Converts the clock signal to a float representing the seconds since the last `play` event.
pub mod clock_to_val;
pub use clock_to_val::*;

///Simple FM-Synthesizer
pub mod fm_synth;
pub use fm_synth::*;


///[Cpal](https://crates.io/crates/cpal) based audio producer and consumer node.
/// supports Linux, Windows and Mac.
pub mod cpal_io;
pub use cpal_io::*;

/*
///Delay node that takes an audio buffer safes samples at some time and releases them after a while.
pub mod delay;
pub use delay::*;
*/

///Mixes two signals into one
pub mod mix;
pub use mix::*;


///Contains a node that converts a left and right mono channel to one stereo signal.
pub mod monos_to_stereo;
pub use monos_to_stereo::*;

///Converts a mono channel to a stereo signal by setting its location somewhere between
/// left and right.
pub mod panned_stereo;
pub use panned_stereo::*;

///Presents a wave editor that is played on a certain speed based on the clock
pub mod wave_editor;
pub use wave_editor::*;
