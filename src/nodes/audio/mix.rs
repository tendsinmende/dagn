//! Mixes two audiobuffer based on a normalized value.

use dager::AbstAggregator;
use dager::Aggregator;
use dager::Executor;
use dager::Node;
use neith::{com::MutVal, io::{Label, Slider}};
use neith::io::TextEditable;
use crate::abst::audio::AudioBuffer;
use crate::nodes::IntoUiNode;
use crate::nodes::NodeCategory;
use crate::abst::misc::NormVal;
use crate::abst::audio::Sample;
use crate::nodes::PortInfo;
use crate::nodes::UiNode;
use crate::nodes::void_generator;
use std::sync::Mutex;
use std::sync::Arc;

pub struct Mix{
    default_alpha: MutVal<NormVal>
}

impl Node for Mix{
    type InSig = (AudioBuffer, AudioBuffer, NormVal);
    type OutSig = [AudioBuffer; 1];

    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (mut a1, a2, mix) = input;
	
	println!("Mixed with @={}", mix.0);
	for (s1, s2) in a1.samples_mut().iter_mut().zip(a2.samples().iter()){
	    *s1 = (*s1 * mix.0 as Sample) + (s2 * (1.0 - mix.0 as Sample));
	}
	[a1]
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator) {
	aggregator.set_default_value(2, Box::new(*self.default_alpha.get()));
    }
}

impl IntoUiNode for Mix{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>{

	let default_lerp_val = MutVal::new(NormVal(0.5));
	let node: Arc<Mutex<dyn AbstAggregator + Send>> = Arc::new(Mutex::new(Aggregator::from_node(Mix{default_alpha: default_lerp_val.clone()})));

	let mut in_ports = PortInfo::from_nodes_in(&*node.lock().unwrap());
	//make the lerp changeable
	in_ports[2].shared_default_value = Some(Box::new(
	    Slider::new(NormVal(0.0), NormVal(0.0), NormVal(1.0), NormVal(0.0125))
		.with_mut_val(default_lerp_val)
		.with_label(Label::new("Lerp"), Some(" %"))
	));

	in_ports[0].name = "In 1".to_string();
	in_ports[1].name = "In 2".to_string();
	in_ports[2].name = "lerp".to_string();

	let mut out_ports = PortInfo::from_nodes_out(&*node.lock().unwrap());
	out_ports[0].name = "Out".to_string();


	Some(UiNode{
	    node,
	    interface: void_generator(),
	    title: "Mix".to_string(),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}


