use std::sync::{Arc, Mutex};

use dager::{AbstAggregator, Aggregator, Executor, Node};
use neith::{com::MutVal, io::{Label, Slider}};

use crate::{abst::{audio::{AudioBuffer, StereoChannel}, misc::NormVal}, nodes::{IntoUiNode, NodeCategory, PortInfo, UiNode, void_generator}};




///Converts two audio buffer into an StereoSignal
pub struct PannedStereo{
    panning: MutVal<NormVal>
}
impl Node for PannedStereo{
    type InSig = (AudioBuffer, NormVal);
    type OutSig = [StereoChannel; 1];

    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (inp, panning) = input;
	let mut l = inp;
	let mut r = l.clone();

	for s in r.samples_mut(){
	    *s *= panning.0 as f32;
	}
	let inv_panning = 1.0 - panning.0 as f32;

	for s in l.samples_mut(){
	    *s *= inv_panning;
	}	
	
	[StereoChannel{buffers: [l, r]}]
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator) {
	aggregator.set_default_value(1, Box::new(*self.panning.get()));
    }
}

impl IntoUiNode for PannedStereo{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let panning_share = MutVal::new(NormVal(0.5));
	let node_agg = Aggregator::from_node(PannedStereo{panning: panning_share.clone()});

	let mut in_ports = PortInfo::from_nodes_in(&node_agg);
	in_ports[0].name = "Input".to_string();
	in_ports[1].name = "Panning".to_string();
	in_ports[1].shared_default_value = Some(Box::new(
	    Slider::new(NormVal(0.5), NormVal(0.0), NormVal(1.0), NormVal(0.05))
		.with_mut_val(panning_share)
		.with_label(Label::new(""), Some(" l/r"))
	));

	let mut out_ports = PortInfo::from_nodes_out(&node_agg);
	out_ports[0].name = "Stereo".to_string();
	
	Some(UiNode{
	    node: Arc::new(Mutex::new(node_agg)),
	    interface: void_generator(),
	    title: "Panned Stereo".to_string(),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
