use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;

use dagxecutor::Aggregator;
use dagxecutor::Executor;
use dagxecutor::Node;
use neith::prelude::MutVal;
use neith::prelude::Widget;

use crate::abst::audio::AudioBuffer;
use crate::abst::audio::Clock;
use crate::abst::audio::Sample;
use crate::abst::audio::Time;
use crate::abst::storage::RingBuffer;
use crate::nodes::DagnNode;
use crate::nodes::DefaultCreator;
use crate::nodes::NodeCategory;
use crate::nodes::NodeCreator;
use crate::nodes::UiNode;

use log::info;

///Simple delay for an audio buffer.
///TODO set characteristic of the sampling via spline editor.
pub struct Delay{
    delay_widget: MutVal<usize>,

    last_sample_rate: RwLock<usize>,
    
    max_delay_time: Time,
    buffer: Mutex<RingBuffer<Sample>>
}

impl Node for Delay{
    type Inputs = [AudioBuffer; 1];
    type Outputs = [AudioBuffer; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let [mut original_buffer] = input;

	//Now build the new buffer
	let mut dbuffer_lock = self.buffer.lock().unwrap();
	for s in original_buffer.samples().iter(){
	    dbuffer_lock.push(*s);
	}

	let sampling_time = *self.delay_widget.get() - original_buffer.buffer_size();
	for i in 0..original_buffer.buffer_size(){
	    let new_sample = if let Some(new_signal) = dbuffer_lock.get(i + sampling_time){
		*new_signal
	    }else{
		println!("Sampling at {} with max @ {}", i, dbuffer_lock.len());
		println!("FAIL");
		0.0
	    };

	    original_buffer.set_sample(i, new_sample);
	}
	[original_buffer]
    }

    fn debug_info(&self) -> &str {
	"AudioDelay"
    }
}

impl DagnNode for Delay{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {

	let max_delay_time = MutVal::new(0.5);
	
	 Box::new(DelayCreator{
	     interface: Arc::new(RwLock::new(
		 neith::layout::Split::new()
		     .with_one(neith::io::Label::new("An echo sometimes sounds like a reverb. However on a technical basis its much easier. It buffers the audio that went through it and samples it at different times."))
		     .with_two(
			 neith::layout::Split::new()
			     .with_one(neith::io::Label::new("Max delay time (in sec.)"))
			     .with_two(
				 neith::io::Slider::new(0.5, 0.01, 2.0, 0.0625)
				     .with_mut_val(max_delay_time.clone())
				     .with_label(neith::base::Text::new(), Some(" Sec."))
			     )
		     )
	     )),
	     max_delay_time
	 })
    }

    fn tags() -> Vec<String> {
	vec![
	    "Audio".to_string(),
	    "Delay".to_string(),
	    "Time".to_string(),
	    "Reverb".to_string(),
	]
    }

    fn name() -> String {
	"Delay".to_string()
    }
}

struct DelayCreator{
    interface: Arc<RwLock<dyn Widget + Send + Sync>>,
    max_delay_time: MutVal<Time>
}

impl NodeCreator for DelayCreator{
    fn create_node(&self, _executor: Arc<Executor>) -> Option<UiNode> {

	let delay_time = MutVal::new(200_000);
	
	let interface = Arc::new(RwLock::new(
	    neith::layout::Split::new()
		.with_one(neith::io::Label::new("Delay in sec."))
		.with_two(
		    neith::io::Slider::new(200, 1, 200_000, 1)
			.with_mut_val(delay_time.clone())
			.with_label(neith::base::Text::new(), Some(" sample"))
		)
		
	));

	let node = Delay{
	    delay_widget: delay_time,

	    last_sample_rate: RwLock::new(1),
	    
	    max_delay_time: *self.max_delay_time.get(),
	    buffer: Mutex::new(RingBuffer::new(200_000)), //Get recreated in the first loop
	};
	
	
	Some(UiNode{
	    node: Aggregator::from_node(node),
	    interface: Some(interface),
	    title: Some(String::from("Delay")),
	    category: NodeCategory::Audio,
	    in_port_names: vec!["Audio Buffer", "Clock"],
	    out_port_names: vec!["Audio Buffer", "Clock"]
	})
    }

    fn creator_interface(&self) -> Arc<RwLock<dyn Widget + Send + Sync>> {
	self.interface.clone()
    }
}
