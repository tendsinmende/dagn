use std::sync::{Arc, Mutex};

use dager::{Aggregator, Executor, Node};

use crate::{abst::audio::{AudioBuffer, StereoChannel}, nodes::{IntoUiNode, NodeCategory, PortInfo, UiNode, void_generator}};




///Converts two audio buffer into an StereoSignal
pub struct MonosToStereo;
impl Node for MonosToStereo{
    type InSig = (AudioBuffer, AudioBuffer);
    type OutSig = [StereoChannel; 1];

    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (l, r) = input;

	[StereoChannel{buffers: [l, r]}]
    }
}

impl IntoUiNode for MonosToStereo{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let node_agg = Aggregator::from_node(MonosToStereo);

	let mut in_ports = PortInfo::from_nodes_in(&node_agg);
	in_ports[0].name = "Left".to_string();
	in_ports[1].name = "Right".to_string();
	let mut out_ports = PortInfo::from_nodes_out(&node_agg);
	out_ports[0].name = "Stereo".to_string();
	
	Some(UiNode{
	    node: Arc::new(Mutex::new(node_agg)),
	    interface: void_generator(),
	    title: "MonosToStereo".to_string(),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
