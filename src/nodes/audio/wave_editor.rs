use std::sync::{Arc, Mutex};

use dager::{AbstAggregator, Aggregator, Executor, Node};
use log::info;
use neith::{base::SharedWidget, com::MutVal, io::{Label, Slider}, widget::Widget};
use neith_widgets::graph::{Graph, SplineEditor};

use crate::{abst::audio::{AudioBuffer, Clock, Time}, nodes::{IntoUiNode, PortInfo, UiNode}};

///Generates a sound wave based on the set spline
pub struct WaveEditor{
    editor: SharedWidget<SplineEditor>,
    //current location on the spline
    time: f32,
    //playback speed in Hz
    speed: MutVal<usize>
}


impl Node for WaveEditor{
    type InSig = (AudioBuffer, Clock, usize);
    type OutSig = (AudioBuffer, Clock);

    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	info!("Start working");
	let (mut buffer, clock, speed) = input;
	
	match clock{
	    Clock::Start | Clock::Pause => {},
	    Clock::Stop => {
		self.time = 0.0; //reset time on stop
	    },
	    Clock::NewFrame => {
		let mut sample_time = self.time;
		let time_per_sample = buffer.duration() / buffer.buffer_size() as Time;
		let step_per_sample = time_per_sample * speed as Time;


		//Iter through time steps and sample values
		self.editor.on_widget(|w| {
		    for s in buffer.samples_mut(){

			*s = w.get_val_at(sample_time).unwrap_or(0.0);

			sample_time = (sample_time + step_per_sample as f32) % 1.0;
		    }
		});
		
		//overwrite stamp for next process call 
		self.time = (sample_time + step_per_sample as f32) % 1.0;
	    }
	}
	
	info!("end working");
	(buffer, clock)
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator) {
	aggregator.set_default_value(2, Box::new(*self.speed.get()));
    }
}

impl IntoUiNode for WaveEditor{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {

	let editor = SharedWidget::new(
	    SplineEditor::new(
		(0.0, -1.0), (1.0, 1.0)
	    ).with_protected_keys(vec![
		    (0.0, 0.0),
		    (1.0, 0.0)
		]).with_free_keys(vec![
		    (0.25, 1.0),
		    (0.75, -1.0)
		])
	);

	let playback_speed = MutVal::new(1);

	let node = Aggregator::from_node(WaveEditor{
	    editor: editor.clone(),
	    time: 0.0,
	    speed: playback_speed.clone()
	});

	let interface = Box::new(move |_ex|{
	    let editor = editor.clone();
	    let interface: Box<dyn Widget + Send + 'static> = Box::new(
		editor
	    );
	    interface
	});

	let mut in_ports = PortInfo::from_nodes_in(&node);
	in_ports[0].name = "AudioBuffer".to_string();
	in_ports[1].name = "Clock".to_string();
	in_ports[2].name = "PlaybackSpeed".to_string();
	in_ports[2].shared_default_value = Some(Box::new(
	    Slider::new(1, 1, 22050, 1)
		.with_mut_val(playback_speed)
		.with_label(Label::new(""), Some(" Hz"))
	));
	
	let out_ports = PortInfo::from_nodes_out(&node);
		

	Some(UiNode{
	    node: Arc::new(Mutex::new(node)),
	    interface,
	    title: "WaveEditor".to_string(),
	    category: crate::nodes::NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
