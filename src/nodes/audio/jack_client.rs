//! # Jack Node
//! The Jack node covers the realtime input/output handling of sound (and midi) information. Therefore, when adding it to a graph a 
//! jack client configuration is setup which is registered at the jack server. Within DagN the node sends out processing requests
//! as a Clock or Midi event. 
//!
//! # Error handling and too long graph execution time
//! Whenever the graph does not finish processing the event in time, the original signal, or a dummy signal is send back to the jack
//! server. The error will appear in the logs.
//!


use dagxecutor::{ProducerAble, Node, Splitter, Executor, Aggregator};
use crate::abst::audio::{AudioBuffer, Clock, AudioState};
use crate::nodes::NodeCategory;
use crate::{nodes::{NodeCreator, DagnNode, DefaultCreator, UiNode}, crossbeam_channel};
use log::{warn, error};
use std::{sync::{RwLock, Arc, Mutex}, time::{Instant, Duration}, any::Any};
use neith::{layout::Direction, prelude::MutVal};

///Channel type used to send back buffer to the original async jack thread.
type JackChannel = crossbeam_channel::Sender<AudioBuffer>;

///The receiver node Receiver node of jack waits for an audio buffer and sends it back to its parent.
pub struct JackReceiver;
impl JackReceiver{
    pub fn new() -> Self{
	JackReceiver
    }
}

impl Node for JackReceiver{
    type Inputs = (AudioBuffer, JackChannel);
    type Outputs = ();
    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	let (clock, channel) = input;
	if let Err(e) = channel.send_timeout(clock, Duration::from_millis(5)){
	    error!("Failed to send clock back to jack receiver: {:?}", e);
	}
    }
}

impl DagnNode for JackReceiver{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {	
	Box::new(DefaultCreator::new(|executor|{

	    let node = JackReceiver::new();
	    
	    Some(UiNode{
		node: Aggregator::from_node(node),
		interface: None,
		title: Some(String::from("JackConsumer")),
		category: NodeCategory::Audio,
		in_port_names: vec!["Audio Buffer", "Jack Feedback"],
		out_port_names: vec![""]
	    })
	}))
    }

    fn tags() -> Vec<String>{
	vec![
	    "Jack".to_string(),
	    "Audio".to_string(),
	    "Out".to_string(),
	    "Sound".to_string(),
	    "Interface".to_string(),
	]
    }
    
    fn name() -> String{
	"JackReceiver".to_string()
    }
}

///The main Jack node that receives 
pub struct JackNode{
    async_client: Option<Arc<Mutex<dyn Any + Send>>>,
    //jack node consumer that are used to communicate with the graph
    consumer: Vec<Arc<RwLock<dyn Splitter<DataLayout = (AudioBuffer, Clock, JackChannel)> + Send + Sync>>>,

    ///Current inner audio state of the node.
    inner_state: MutVal<AudioState>,
    ///Is some if a state change needs to be signaled
    signal_state_change: MutVal<Option<AudioState>>,
    
    
    time_print: MutVal<String>,
    reftime_print: MutVal<String>,
    load_print: MutVal<String>,

    executor: Arc<Executor>,
}

impl JackNode{
    ///Tries to create the server. If initialisation of the ports fail the node is still created, but must be reconnected to jack from the ui.
    pub fn new(executor: Arc<Executor>) -> Option<Arc<RwLock<Self>>>{

	
	let time_print = MutVal::new(String::from("Not Time Yet!"));
	let reftime_print = MutVal::new(String::from("Not Time Yet!"));
	let load_print = MutVal::new(String::from("Not Load Yet!"));

	let tp = time_print.clone();
	let rtp = reftime_print.clone();
	let lp = load_print.clone();

	//The Node within this graph, shared with the process as well
	let node = Arc::new(RwLock::new(JackNode{
	    async_client: None,
	    consumer: Vec::new(),
	    
	    inner_state: MutVal::new(AudioState::Stopped),
	    signal_state_change: MutVal::new(None),
	    
	    time_print,
	    reftime_print,
	    load_print,
	    executor
	}));
	
	//Currently just register on output, later derive from configuration.
	let (client, status) = match jack::Client::new("Dagn_jack_out", jack::ClientOptions::NO_START_SERVER){
	    Ok(sc) => sc,
	    Err(e) => {
		error!("Failed to create jack client, is the jack sound server running?");
		return None;
	    }
	};

	let mut out_port = client.register_port("DagnOut", jack::AudioOut::default()).expect("Failed to activate jack out port");

	let async_node = node.clone();
	//define jack process
	let process = jack::ClosureProcessHandler::new(move |client: &jack::Client, ps: &jack::ProcessScope| -> jack::Control{
	    //get jacks requirements for this buffer, then send buffer into graph
	    let sample_rate = client.sample_rate() as usize;
	    let size = client.buffer_size() as usize;
	    let frame_time = ps.last_frame_time();

	    *tp.get_mut() = format!("jack time: {}", frame_time);
	    *rtp.get_mut() = format!("Ref time: {}", Instant::now().elapsed().as_secs_f32());
	    *lp.get_mut() = format!("CPULoad: {}%", client.cpu_load());



	    //Check if we've got a state change. If thats the case, change the inner state and send a signal accordingly through the
	    // graph.
	    if let Some(changed_state) = async_node.read().unwrap().signal_state_change.get_mut().take(){

		//Send change to clients
		for cons in &async_node.read().unwrap().consumer{
		    
		    //Build frame buffer
		    let audio_buffer = AudioBuffer::new(size, sample_rate);
		    //build receiver channel that gets send back by the (hopefully) connected JacakReceiver
		    let (sender, _receiver) = crossbeam_channel::bounded::<AudioBuffer>(0);
		    if let Err(e) = cons.read().unwrap().split(
			async_node.read().unwrap().executor.clone(),
			(
			    audio_buffer,
			    changed_state.into_clock(),
			    sender
			),
			"JackNode"
		    ){
			warn!("Graph was not ready for jack audio buffer: {:?}", e);
		    }
		}
		//Save change in inner state
		*async_node.read().unwrap().inner_state.get_mut() = changed_state;
	    }

	    //If we area playing, send an audio buffer, otherwise just return
	    if *async_node.read().unwrap().inner_state.get() == AudioState::Playing{
		let mut wait_receiver = Vec::new();
		for cons in &async_node.read().unwrap().consumer{

		    //Build frame buffer
		    let audio_buffer = AudioBuffer::new(size, sample_rate);
		    //build receiver channel that gets send back by the (hopefully) connected JacakReceiver
		    let (sender, receiver) = crossbeam_channel::bounded::<AudioBuffer>(100);
		    
		    if let Err(e) = cons.read().unwrap().split(
			async_node.read().unwrap().executor.clone(),
			(
			    audio_buffer,
			    Clock::NewFrame,
			    sender
			),
			"JackNode"
		    ){
			warn!("Graph was not ready for jack audio buffer: {:?}", e);
		    }
		    
		    //Push into wait list
		    wait_receiver.push(receiver);
		}

		//Now wait until timeout, if we've got something, write the buffer into the out connection and return.
		//Get buffer we want to target
		let out = out_port.as_mut_slice(&ps);

		if wait_receiver.len() > 0{
		    if let Ok(audio_buffer) = wait_receiver[0].recv_timeout(Duration::from_millis(16)){
			let (buffer, _rate) = audio_buffer.dissolve();
			for (idx, s) in out.iter_mut().enumerate(){
			    *s = buffer[idx];
			}
		    }else{
			warn!("Got no buffer back!");
		    }
		}
	    }else{
		//Not playing, set "nothing"
		let out = out_port.as_mut_slice(&ps);
		for sample in out.iter_mut(){
		    *sample = 0.0;
		}
	    }

	    jack::Control::Continue
	});

	let active_client = client.activate_async((), process).unwrap();
	node.write().unwrap().async_client = Some(Arc::new(Mutex::new(active_client)));
	Some(node)
    }
}

impl ProducerAble for JackNode{
    type OutputLayout = (AudioBuffer, Clock, JackChannel);

    fn receiver_channel(&mut self, channel: Arc<RwLock<dyn Splitter<DataLayout = Self::OutputLayout> + Send + Sync>>){
	println!("Got Consumer!");
	self.consumer.push(channel);
    }
    
    fn produce(&self) {
	//Does nothing since we handle that in the ClosueProcessHandler
    }
}

impl Node for JackNode{
    type Inputs = ();
    type Outputs = (AudioBuffer, Clock, JackChannel);
    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	panic!("Jack should not produce!");
    }
}

impl DagnNode for JackNode{
    fn creator() -> Box<dyn NodeCreator + Send + Sync> {
	Box::new(DefaultCreator::new(|executor|{

	    let node = JackNode::new(executor)?;
	    
	    let sw1 = node.read().unwrap().signal_state_change.clone();
	    let sw2 = node.read().unwrap().signal_state_change.clone();
	    let sw3 = node.read().unwrap().signal_state_change.clone();
	    
	    let interface = Arc::new(RwLock::new(
		neith::layout::Split::new()
		    .with_one(
			neith::layout::Split::new()
			    .with_one(
				neith::layout::StaticList::new(Direction::Horizontal)
				    .with_items(
					vec![
					    Arc::new(RwLock::new(
						neith::io::Button::new(neith::base::Text::new().with_text("Play"))
						    .with_action(move || *sw1.get_mut() = Some(AudioState::Playing))
					    )),
					    Arc::new(RwLock::new(
						neith::io::Button::new(neith::base::Text::new().with_text("Pause"))
						    .with_action(move || *sw2.get_mut() = Some(AudioState::Paused))
					    )),
					    Arc::new(RwLock::new(
						neith::io::Button::new(neith::base::Text::new().with_text("Stop"))
						    .with_action(move || *sw3.get_mut() = Some(AudioState::Stopped))
					    ))
					]
				    )
			    ).with_two(
				neith::io::Edit::new(neith::base::Text::new())
				    .with_mut_val(node.read().unwrap().time_print.clone())
			    )
		    ).with_two(
			neith::layout::Split::new()
			    .with_one(  
				neith::io::Edit::new(neith::base::Text::new())
				    .with_mut_val(node.read().unwrap().reftime_print.clone())  
			    ).with_two(
				
				neith::io::Edit::new(neith::base::Text::new())
				    .with_mut_val(node.read().unwrap().load_print.clone())
			    )
		    )
	    ));

	    Some(UiNode{
		node: Aggregator::from_arc_producer(node),
		interface: Some(interface),
		title: Some(String::from("JackProducer")),
		category: NodeCategory::Audio,
		in_port_names: vec![""],
		out_port_names: vec!["Audio Buffer", "Clock", "Jack Feedback (must be connected!)"]
	    })
	}))
    }

    fn name() -> String {
	"JackNode".to_string()
    }

    fn tags() -> Vec<String> {
	vec![
	    "Jack".to_string(),
	    "Audio".to_string(),
	    "Out".to_string(),
	    "Sound".to_string(),
	    "Interface".to_string(),
	    "Source".to_string()
	]
    }
}
