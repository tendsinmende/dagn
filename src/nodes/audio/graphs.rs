use crate::nodes::*;

///Collects all possible node types. This might at some point be done at runtime.
pub enum NodeType{
    ClockTick(ClockTick)
}

impl NodeType{
    pub fn is_producer(&self) -> bool{
	match self{
	    NodeType::ClockTick(_) => true,
	}
    }
}
