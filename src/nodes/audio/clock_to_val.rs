//!Transforms some clock into seconds as floats since start of the clock.

use std::{sync::{Arc, Mutex}, time::Instant};

use dager::{Aggregator, Executor, Node};
use neith::{com::MutVal, widget::Widget};

use crate::{abst::audio::Clock, nodes::{IntoUiNode, NodeCategory, PortInfo, UiNode}};


pub struct ClockToVal{
    start: Option<Instant>,
    last_val: MutVal<String>,
}

impl Node for ClockToVal{
    type InSig = [Clock; 1];
    type OutSig = (f64, Clock);
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let [clock] = input;
	match clock{
	    Clock::NewFrame => {
		if let Some(start) = &self.start{
		    let secs = start.elapsed().as_secs_f64();
		    *self.last_val.get_mut() = format!("Value: {}", secs);
		    (secs, clock)
		}else{
		    *self.last_val.get_mut() = format!("Value: {}", 0.0);
		    (0.0, clock)
		}
	    },
	    Clock::Start => {
		self.start = Some(Instant::now());
		*self.last_val.get_mut() = format!("Value: {}", 0.0);    
		(0.0, clock)
	    },
	    Clock::Stop => {
		self.start = None;
		*self.last_val.get_mut() = format!("Value: {}", 0.0);
		(0.0, clock)
	    },
	    Clock::Pause => {
		//TODO actually save the when we paused, and continue when play event comes again
		self.start = None;
		*self.last_val.get_mut() = format!("Value: {}", 0.0);
		(0.0, clock)	
	    }
	}
    }
}

impl IntoUiNode for ClockToVal{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let node = ClockToVal{
	    start: None,
	    last_val: MutVal::new(format!("Value: {}", 0.0))
	};

	let valcp = node.last_val.clone();
	let ui_creator = Box::new(move |_ex|{
	    let valcp = valcp.clone();
	    let widget: Box<dyn Widget + Send + 'static> = Box::new(
		neith::io::Label::new("Time").with_mut_val(valcp)
	    );

	    widget
	});

	let aggregator = Aggregator::from_node(node);
	let in_ports = PortInfo::from_nodes_in(&aggregator);
	let out_ports = PortInfo::from_nodes_out(&aggregator);
	
	Some(UiNode{
	    node: Arc::new(Mutex::new(aggregator)),
	    interface: ui_creator,
	    title: String::from("ClockToValue"),
	    category: NodeCategory::None,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
