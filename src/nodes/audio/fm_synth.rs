
use std::sync::{RwLock, Arc, Mutex};

use dager::{AbstAggregator, Aggregator, Executor};
use dager::Node;
use neith::{com::MutVal, io::{Label, Slider}, layout::{Direction, Split, split::SplitType}, widget::Widget};

use crate::{abst::audio::AudioBuffer, nodes::{NodeCategory, PortInfo}};
use crate::nodes::IntoUiNode;
use crate::nodes::UiNode;

pub struct FMSynth{
    current_value: MutVal<String>,
    default_carrier: MutVal<f64>,
    default_modulator: MutVal<f64>,
    carrier_phase: f64,
    modulator_phase: f64,
    ///Modulator level of this Synth, usually "Beta" in the formulas.
    mod_level: MutVal<f64>,
}

impl FMSynth{
    pub fn new() -> Self{
	FMSynth{
	    current_value: MutVal::new(String::from("No Buffer Yet!")),
	    default_carrier: MutVal::new(440.0),
	    default_modulator: MutVal::new(10.0),
	    carrier_phase: 0.0,
	    modulator_phase: 0.0,
	    mod_level: MutVal::new(1.0),
	}
    }
}

impl Node for FMSynth{
    //Clock and frequency
    type InSig = (AudioBuffer, f64, f64);
    //Outputs Clock with sinus in buffer as well as the last value that was set.
    type OutSig = [AudioBuffer; 1];

    ///Outputs sinus in Clocks's buffer, and the last value on the f64 that was calculated. However, both are only send when the clocks value is
    /// Clock::NewFrame. Otherwise the clocksignal and 0.0 are send.
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (mut buffer, carrier_freq, modulator_freq) = input;
	let sr = buffer.sample_rate() as f64;
	
	//Per sample, advance phase and push inside the sin value
	for sample in buffer.buffer.iter_mut(){
	    //Advance phase of modulator
	    self.modulator_phase += modulator_freq * 2.0 * std::f64::consts::PI / sr;

	    //Advance phase of carrier
	    self.carrier_phase += self.modulator_phase.cos() * *self.mod_level.get() * carrier_freq * 2.0 * std::f64::consts::PI / sr;
	    
	    
	    *sample = self.carrier_phase.cos() as f32;
	}
	
	let last_val = buffer.buffer[buffer.buffer.len() - 1] as f64; //Get last value and send as well
	*self.current_value.get_mut() = format!("Last sample: {}", last_val);
	[buffer]
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator) {
	aggregator.set_default_value(1, Box::new(*self.default_carrier.get()));
	aggregator.set_default_value(2, Box::new(*self.default_modulator.get()));
    }
}



impl IntoUiNode for FMSynth{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode> {
	let node = Arc::new(Mutex::new(FMSynth::new()));
	let default_carrier = node.lock().unwrap().default_carrier.clone();
	let default_modulator = node.lock().unwrap().default_modulator.clone();
	    
	let node_aggreagtor = Arc::new(Mutex::new(Aggregator::from_arc(node.clone())));
	let interface_generator = Box::new(move |ex: Arc<Executor>|{
	    let mod_level = node.lock().unwrap().mod_level.clone();
	    
	    let widget: Box<dyn Widget + Send + 'static> = Box::new(
		Split::new()
		.with_one(
		    Split::new()
			.with_one(Label::new("Modulation Index"))
			.with_two(
			    neith::io::Slider::new(1.0, 0.0125, 2.0, 0.0125)
				.with_mut_val(mod_level)
				.with_label(Label::new("Mod index"), Some("idx"))
			).with_direction(Direction::Horizontal)
			.with_split_type(SplitType::Absolute(50.0))
		)
		.with_two(Label::new("Change the carrier frequency to the the tone. Change the modulator to set the modulation speed of that tone. Finally you can set the influence of the modulator via the \"modulation index\"."))
		.with_split_type(SplitType::Relative(0.6))
		.with_moveable(true)
	    );

	    widget
	});

	let mut in_ports = PortInfo::from_nodes_in(&*node_aggreagtor.lock().unwrap());
	in_ports[0].name = "AudioBuffer".to_string();
	in_ports[1].name = "Carrier Frequency".to_string();
	in_ports[1].shared_default_value = Some(Box::new(
	    Slider::new(1.0, 1.0, 28_000.0, 1.0)
		.with_mut_val(default_carrier)
		.with_label(Label::new("").with_size(10.0), Some(" Hz"))
	));
	in_ports[2].name = "Modulator Frequency".to_string();
	in_ports[2].shared_default_value = Some(Box::new(
	    Slider::new(15.0, 0.0, 100.0, 1.0)
		.with_mut_val(default_modulator)
		.with_label(Label::new("").with_size(10.0), Some(" Hz"))
	));
	let out_ports = PortInfo::from_nodes_out(&*node_aggreagtor.lock().unwrap());
	
	Some(UiNode{
	    node: node_aggreagtor,
	    interface: interface_generator,
	    title: String::from("Simple FMSynth"),
	    category: NodeCategory::Audio,
	    in_ports,
	    out_ports,
	    executor
	})
    }
}
