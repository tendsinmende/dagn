use std::sync::Mutex;
use std::sync::{Arc, RwLock};
use std::{fmt::{Formatter, Display}, any::TypeId};

use dager::AbstAggregator;
use dager::Edge;
use dager::Executor;
use log::error;

///Contains audio related nodes
pub mod audio;
///Contains all nodes that should be included on startup.
pub mod register_table;

/// Contains generic nodes that can be used in a variety of contexts.
pub mod generic;
use neith::base::Void;
use neith::com::MutVal;
use neith::config::Color;
use neith::io::TextEditable;
use neith::widget::Widget;
use neith_widgets::node::ConnectionError;
use neith_widgets::node::NodeContent;

pub struct PortInfo{
    ty_id: TypeId,
    pub name: String,
    pub shared_default_value: Option<Box<dyn Widget + Send + 'static>>
}

impl PortInfo{
    ///Generates a Vector of port infos based on some nodes inputs.
    pub fn from_nodes_in(node: &dyn AbstAggregator) -> Vec<Self>{
	let mut defs = vec![];
	let mut idx = 0;
	while let Some(ty) = node.in_type_id(idx){
	    defs.push(PortInfo{
		ty_id: ty,
		name: String::from("Unnamed"),
		shared_default_value: None
	    });
	    idx += 1;
	}

	defs
    }
    ///Generates a Vector of port infos based on some nodes inputs.
    pub fn from_nodes_out(node: &dyn AbstAggregator) -> Vec<Self>{
	let mut defs = vec![];
	let mut idx = 0;
	while let Some(ty) = node.out_type_id(idx){
	    defs.push(PortInfo{
		ty_id: ty,
		name: String::from("Unnamed"),
		shared_default_value: None
	    });
	    idx += 1;
	}
	defs
    }
}

pub trait IntoUiNode{
    fn into_ui_node(executor: Arc<Executor>) -> Option<UiNode>;   
}

///Closure that produces a interface for some node if called.
type InterfaceGenerator = Box<dyn Fn(Arc<Executor>) -> Box<dyn Widget + Send + 'static> + Send>;

pub fn void_generator() -> InterfaceGenerator {
    Box::new(|_ex| Box::new(Void::new()))
}

///Ui node wrapper, that reacts depending on the implemented traits of the original data node
pub struct UiNode {
    //Data node that is used
    pub node: Arc<Mutex<dyn AbstAggregator + Send>>,
    //reference to this nodes interface
    pub interface: InterfaceGenerator,
    pub title: String,
    pub category: NodeCategory,
    //collects optional info about some port, like the name and possibly a default shared value
    pub in_ports: Vec<PortInfo>,
    pub out_ports: Vec<PortInfo>,
    pub executor: Arc<Executor>,
}

///Implements NodeContent on every AbstNode that does not provide interface capabilities
impl NodeContent for UiNode{
    type Node = UiNode;
    fn inputs(&self) -> Vec<(TypeId, String)> {
	self.in_ports.iter().map(|pi| (pi.ty_id.clone(), pi.name.clone())).collect()
    }
    fn outputs(&self) -> Vec<(TypeId, String)> {
	self.out_ports.iter().map(|pi| (pi.ty_id.clone(), pi.name.clone())).collect()
    }
    
    fn try_in_connect(&mut self, _input_index: usize, _source: &Self::Node, _source_out: usize) -> Result<(), ConnectionError> {
	//Ignore since the Edge::connect in the out connect should take care of both ends.
	/*
	match self.node.lock(){
	    Ok(ref mut inner_node) => {
		println!("Create in connection");
		//Set in edge for this 
		if let Err(e) = Edge::connect(source.node.clone(), source_out, self.node.clone(), input_index){
		    error!("Failed to set graph connection: {:?}", e);
		    Err(ConnectionError::ConnectionFailed)
		}else{
		    Ok(())
		}
	    },
	    Err(e) => {
		error!("Failed to lock inner node while adding connection");
		Err(ConnectionError::ConnectionFailed)
	    }
	}
	 */
	Ok(())
    }
    
    fn try_out_connect(&mut self, out_index: usize, target: &Self::Node, target_in: usize) -> Result<(), ConnectionError> {
	if let Err(e) = Edge::connect(self.node.clone(), out_index, target.node.clone(), target_in){
	    error!("Failed to set graph connection: {:?}", e);
	    Err(ConnectionError::ConnectionFailed)
	}else{
	    Ok(())
	}	
    }

    fn in_disconnected(&mut self, _in_index: usize) -> Result<(), ConnectionError>{
	Ok(())
    }
    fn out_disconnected(&mut self, out_index: usize) -> Result<(), ConnectionError>{

	match self.node.lock(){
	    Ok(ref mut inner_node) => {
		if let Err(e) = inner_node.remove_out_edge(out_index){
		    error!("Failed to disconnect edge from node in graph: {:?}", e);
		    Err(ConnectionError::DisconnectionFailed)
		}else{
		    Ok(())
		}
	    },
	    Err(e) => {
		error!("Failed to lock inner node while removing connection");
		Err(ConnectionError::DisconnectionFailed)
	    }
	}	 
    }

    fn out_default_value(&mut self, idx: usize) -> Option<Box<dyn Widget + Send + 'static>> {
	if let Some(p) = self.out_ports.get_mut(idx){
	    p.shared_default_value.take()
	}else{
	    None
	}
    }

    fn in_default_value(&mut self, idx: usize) -> Option<Box<dyn Widget + Send + 'static>> {
	if let Some(p) = self.in_ports.get_mut(idx){
	    p.shared_default_value.take()
	}else{
	    None
	}
    
    }
    
    fn get_title(&self) -> Option<String> {
	Some(self.title.clone())
    }

    fn get_interface(&self) -> Option<Box<dyn Widget + Send + 'static>> {
	Some((self.interface)(self.executor.clone()))
    }

    fn frame_color(&self) -> Option<Color>{
	self.category.to_color()
    }
}


///A node can sometimes be put in one of those categories.
///In that case the interface can automatically altered to make the category visible in the UI.
#[derive(Eq,PartialEq, Debug, Hash)]
pub enum NodeCategory{
    ///Audio related nodes like synthesizers
    Audio,
    ///Scalar math related nodes, like a sinus node or simple add/multiply operations.
    ScalarMath,
    ///Debugging utilities
    Debug,
    ///If the node can not be put into one of those.
    None,
}

impl NodeCategory{
    pub fn to_color(&self) -> Option<Color>{
	match self{
	    NodeCategory::Audio => Some(Color::Hex(String::from("d79921"))),
	    NodeCategory::ScalarMath => Some(Color::Hex(String::from("458588"))),
	    NodeCategory::Debug => Some(Color::Hex(String::from("d65d0e"))),
	    NodeCategory::None => None,
	}
    }
}


impl Display for NodeCategory{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
	match self{
	    NodeCategory::Audio => write!(f, "Audio"),
	    NodeCategory::ScalarMath => write!(f, "Scala Math"),
	    NodeCategory::Debug => write!(f, "Debug"),
	    NodeCategory::None => write!(f, "None"),
	}
    }
}
