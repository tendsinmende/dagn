
use std::{sync::Arc, time::Instant};

use cpal::{SupportedOutputConfigs, SupportedStreamConfig, SupportedStreamConfigRange, traits::{DeviceTrait, HostTrait}};
use log::info;
use neith::{base::{ArcWidget, Void}, com::MutVal, io::{DropDown, Label, Slider}, layout::{Align, List}};
use neith::event::Event;
use neith::io::Button;
use neith::layout::Direction;
use neith::layout::Split;
use neith::layout::StaticList;
use neith::layout::split::SplitType;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::widget::DrawCalls;
use neith::widget::Widget;

use core::iter::Iterator;

use crate::config::DagnConfig;

use super::ConfigAction;

#[derive(Clone)]
enum DeviceConfiguratorAction{
    Save{
	dev_idx: usize,
	dev_range_idx: usize,
	sampling: usize
    }
}

struct CpalDeviceConfigurator{
    layout: Split,
    exchange_layout: ArcWidget,
    //Stores all valid configs
    config_store: Arc<Vec<(String, Vec<SupportedStreamConfigRange>)>>
}

impl CpalDeviceConfigurator{
    fn new() -> Self{

	//Right side of the dialog, used to set the range based on the previously selected device.
	let context = ArcWidget::new(Label::new("Select a device, to choose its configuration"));

	let config_store: Arc<Vec<(String, Vec<SupportedStreamConfigRange>)>> = Arc::new(
	    cpal::default_host().devices().unwrap().into_iter().filter_map(|dev|{
		if let Ok(configs) = dev.supported_output_configs(){
		    let ranges = configs.into_iter()
			.map(|r| r).collect::<Vec<_>>();

		    Some((dev.name().unwrap(), ranges))
		}else{
		    info!("Lost device {}", dev.name().unwrap());
		    None
		}
	    }).collect());

	let storecp = config_store.clone();


	let per_device_config_list = config_store.iter().map(|(d, configs)| {
	    let widget: Box<dyn Widget + Send + 'static> = Box::new(
		Button::new(&d)
		    .with_action({
			let ctxcp = context.clone();
			let device_name = d.clone();

			//The device based configuration options. How many channels there are, and what the sampling range is.
			// In general there are two types of configurations
			// 1. Simple one sample config and one channel config,
			// 2. Several sampling configurations per one channel config.

			//Based on that information we create a list that might feature an slider for the sample range and
			//the channel number 
			
			// by clicking the "use" button, the currently set configuration is written to the global config.

			
			let mut device_config: Box<dyn Widget + Send + 'static> = Box::new(List::new(Direction::Vertical)
			    .with_items(
				configs.into_iter().map(|config|{

				    let service_name = device_name.clone();
				    let target_sampling = config.min_sample_rate().0 as usize;
				    let target_channel = config.channels() as usize;
				    
				    
				    let config_range: Box<dyn Widget + Send + 'static> = if config.min_sample_rate().0 == config.max_sample_rate().0{
					Box::new(
					    Split::new()
						.with_one(
						    Split::new()
							.with_one(
							    Align::new(Label::new(&format!("Sample count: {}", config.min_sample_rate().0)))
							).with_two(
							    Align::new(Label::new(&format!("Channel count: {}", config.channels())))
							).with_direction(Direction::Vertical)
						).with_two(
						    Button::new("Use")
							.with_action({
							    move || {
								let service_name = service_name.clone();
								let mut old_config = DagnConfig::get();
								old_config.cpal_config.out_service_name = service_name;
								old_config.cpal_config.out_target_sampling = target_sampling;
								old_config.cpal_config.out_target_channels = target_channel;

								DagnConfig::set(old_config);
							    }
							})
						).with_direction(Direction::Vertical)
						.with_split_type(SplitType::Absolute(100.0))
						.with_inv_reference()
					)
				    }else{
					let shared_sample_rate_value = MutVal::new(config.min_sample_rate().0);
					Box::new(
					    Split::new()
						.with_one(
						    Split::new()
							.with_one(
							    Split::new()
								.with_one(Align::new(Label::new("Sample Count")))
								.with_two(
								    Slider::new(
									config.min_sample_rate().0,
									config.min_sample_rate().0,
									config.max_sample_rate().0.min(4096),
									1
								    ).with_mut_val(shared_sample_rate_value.clone())
									.with_label(Label::new(""), Some(" Samples"))
								)
							).with_two(
							    Align::new(Label::new(&format!("Channel count: {}", config.channels())))
							).with_direction(Direction::Vertical)
						).with_two(
						    Button::new("Use")
							.with_action({
							    move || {
								let service_name = service_name.clone();
								let target_sampling = *shared_sample_rate_value.get() as usize;
								let mut old_config = DagnConfig::get();
								old_config.cpal_config.out_service_name = service_name;
								old_config.cpal_config.out_target_sampling = target_sampling;
								old_config.cpal_config.out_target_channels = target_channel;

								DagnConfig::set(old_config);
							    }
							})
						).with_direction(Direction::Vertical)
						.with_split_type(SplitType::Absolute(100.0))
						.with_inv_reference()
					)
				    };
				    config_range
				}).collect::<Vec<_>>()
			    ).with_element_size(50.0));

			move || {			    
			    //Create a widget for each configuration range, let the user decide by clicking on "use"
			    
			    ctxcp.exchange(&mut device_config);
			}
		    })
	    );
	    widget
	}).collect::<Vec<_>>();

	
	let layout = Split::new()
	    .with_one(
		List::new(Direction::Vertical)
		    .with_items(per_device_config_list)
	    ).with_two(
		context.clone()
	    ).with_direction(Direction::Vertical)
	    .with_split_type(SplitType::Absolute(150.0))
	    .with_moveable(true);
	
	CpalDeviceConfigurator{
	    layout,
	    exchange_layout: context,
	    config_store: storecp
	}
	
    }
}

impl Widget for CpalDeviceConfigurator{
        fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	self.layout.on_event(event, location)
    }

    fn area(&self) -> Area {
	self.layout.area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &neith::Interface, level: usize) -> DrawCalls {	
	self.layout.get_calls(host_extent, draw_time, interface, level)
    }
}

pub struct ConfigOverlay{
    layout: Split
}

impl ConfigOverlay{
    pub fn new() -> Self{

	let shared_frame = ArcWidget::new(Align::new(Label::new("Choose sub menu")));
	let general_shared_frame = ArcWidget::new(Align::new(Label::new("Choose General config button")));
	let gsf_cp = general_shared_frame.clone();
	let mut cpal_device_chooser: Box<dyn Widget + Send + 'static> = Box::new(
	    CpalDeviceConfigurator::new()
	);
	
	
	
	let mut general: Box<dyn Widget + Send + 'static> = Box::new(
	    Split::new()
		.with_one(
		    List::new(Direction::Vertical)
			.with_items(vec![
			    Box::new(
				Button::new("Cpal Output Device")
				    .with_action(move || {
					gsf_cp.exchange(&mut cpal_device_chooser);
				    })
			    ),
			])
			.with_element_size(50.0)
		).with_two(
		    general_shared_frame
		).with_direction(Direction::Horizontal)
		.with_split_type(SplitType::Absolute(200.0))
		.with_moveable(true)
		.with_inv_reference()
	);

	
	let chooser_list = List::new(Direction::Vertical)
	    .with_items(vec![{
		let cloned_frame = shared_frame.clone();
		Box::new(Button::new("Audio").with_action(move || cloned_frame.exchange(&mut general)))
	    }
	]);
	
	let layout = Split::new()
	    .with_one(
		chooser_list
	    ).with_two(
		shared_frame
	    ).with_split_type(SplitType::Absolute(120.0))
	    .with_direction(Direction::Vertical);

	ConfigOverlay{
	    layout
	}
    }
}


impl Widget for ConfigOverlay{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	self.layout.on_event(event, location)
    }

    fn area(&self) -> Area {
	self.layout.area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &neith::Interface, level: usize) -> DrawCalls {
	self.layout.get_calls(host_extent, draw_time, interface, level)
    }
}
