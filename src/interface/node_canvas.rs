use std::collections::HashMap;
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;
use std::time::Instant;

use dager::Executor;
use log::warn;
use neith::Interface;
use neith::base::ArcWidget;
use neith::base::Frame;
use neith::com::MutVal;
use neith::event::Event;
use neith::event::Key;
use neith::event::KeyCode;
use neith::event::KeyState;
use neith::event::MouseButton;
use neith::event::WindowEvent;
use neith::io::Button;
use neith::io::Edit;
use neith::io::Label;
use neith::layout::Align;
use neith::layout::Direction;
use neith::layout::List;
use neith::layout::Split;
use neith::layout::split::SplitType;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::widget::DrawCalls;
use neith::widget::Widget;
use neith_widgets::node::NodeCanvas;

use crate::nodes::IntoUiNode;
use crate::nodes::NodeCategory;
use crate::nodes::UiNode;
use crate::nodes::audio::Mix;
use crate::nodes::register_table;

///Some creator function. Used to setup a UiNode based on some type. Might use the executor to spawn
/// a thread that activates the note from time to time.
pub type CreatorFunc = Box<dyn Fn(Arc<Executor>) -> Option<UiNode> + Send>;

const OVERLAY_EXTENT: Vector2<Unit> = Vector2{x: 400.0, y: 300.0};

///Structure that organizes nodes into categories and saves their creator functions.
pub struct CreatorStore{
    ///All creator functions and the name of the produced nodes
    nodes: Vec<(String, CreatorFunc)>,
    ///Index structure for the category of those nodes.
    ///Holds: (Category, all indices of nodes in this category)
    categories: HashMap<NodeCategory, Vec<usize>>,
}
impl CreatorStore{
    ///Initializes the whole CreatorStore. 
    fn new() -> Self{
	//TODO create macro that iterates over all nodes in the nodes directory and calls the static
	// `creator()` function or something like that.
	let mut creator = CreatorStore{
	    nodes: vec![],
	    categories: HashMap::new()
	};

	//load hardcoded register_table
	register_table::register_table(&mut creator);
	
	creator
    }

    pub fn add(&mut self, name: String, category: NodeCategory, creator: CreatorFunc){
	let idx = self.nodes.len();
	self.nodes.push((name, creator));
	if let Some(idxstore) = self.categories.get_mut(&category){
	    idxstore.push(idx);
	}else{
	    self.categories.insert(category, vec![idx]);  
	}
    }
}

//Our creator overlay that can be spawned
struct CreatorOverlay{
    layout: Frame
}

impl CreatorOverlay{
    ///Creates a new overlay based on the current store overlay.
    fn from_store(
	store: &CreatorStore,
	executor: Arc<Executor>,
	//The slot within the overlay struct. We'll add the node there when some node is clicked.
	add_slot: Arc<Mutex<Option<usize>>>
    ) -> Self{

	let shared_chooser = ArcWidget::new(Align::new(Label::new("Choose a Node category")));
	let chooser_list = List::new(Direction::Vertical)
	    .with_items(store.categories.keys().map(|k| {
		
		//Create the inner widget that gets exchanged 
		let mut node_ty_buttons: Box<dyn Widget + Send> = Box::new(List::new(Direction::Vertical).with_items(store.categories.get(k).unwrap_or(&vec![]).iter().map(|node_idx|{

		    let name = store.nodes[*node_idx].0.clone();
		    let idx = *node_idx;
		    let slot_cpy = add_slot.clone();
		    let button: Box<dyn Widget + Send> = Box::new(
			Button::new(&name)
			    .with_action(move|| {
				*slot_cpy.lock().unwrap() = Some(idx);
			    })
		    );
		    button
		}).collect::<Vec<Box<dyn Widget + Send>>>()));

		let cloned_arc = shared_chooser.clone();
		
		//The store button
		let button: Box<dyn Widget + Send + 'static> = Box::new(
		    Button::new(&k.to_string())
			.with_action(move || cloned_arc.exchange(&mut node_ty_buttons))
		);
		
		button
	    }).collect());
	
	CreatorOverlay{
	    layout: Frame::new(
		Split::new()
		    .with_one(Edit::new("Search...".to_string()))
		    .with_two(
			Split::new()
			    .with_one(chooser_list)
			    .with_two(shared_chooser)
			    .with_direction(Direction::Vertical)
		    ).with_direction(Direction::Horizontal)
		    .with_split_type(SplitType::Absolute(30.0))
	    )
	}
    }
}

impl Widget for CreatorOverlay{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {

	/*
	match event{
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::A, state: KeyState::Pressed})) => {
		self.inner_canvas.add_node_at(Mix::into_ui_node(), *location, (10.0, 10.0).into());
	    },
	    _ => {}
	}
	 */
	
	//Push down into canvas
	self.layout.on_event(event, location)
    }

    fn area(&self) -> Area {
	self.layout.area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls {
	self.layout.get_calls(host_extent, draw_time, interface, level)
    }
}

enum OverlayAction{
    ///Open overlay at location
    Open(Vector2<Unit>),
    Close,
    None,
}

impl OverlayAction{
    ///takes whatever the action is, returns it and set self to `none`.
    fn take(&mut self) -> Self{
	let mut none_action = OverlayAction::None;
	std::mem::swap(self, &mut none_action);
	none_action
    }
}

///Wrapper around neiths canvas to control additional functionality
pub struct DagnCanvas{
    ///The threadpool used by the inner dager nodes per node.
    executor: Arc<Executor>,
    inner_canvas: NodeCanvas<UiNode>,
    ///true if mod is pressed
    is_mod: bool,

    ///Some, if a node should be added based on the overlay.
    add_node: Arc<Mutex<Option<usize>>>,
    
    ///If some, contains an overlay's id as well as the location of this overlay.
    active_overlay: Option<(Vector2<Unit>, usize, CreatorOverlay)>,

    overlay_action: OverlayAction,
    
    ///Saves all creator functions. Used to create the `CreatorOverlay` when pressing super+a.
    /// Might be modified at runtime to add new nodes or loadable custom graphs.
    creator_store: CreatorStore
}

impl DagnCanvas{
    pub fn new() -> Self{

	let creator_store = CreatorStore::new();
	
	DagnCanvas{
	    executor: dager::Executor::new(),
	    inner_canvas: NodeCanvas::new(),
	    is_mod: false,
	    add_node: Arc::new(Mutex::new(None)),
	    active_overlay: None,
	    overlay_action: OverlayAction::None,
	    creator_store
	}
    }
}

impl Widget for DagnCanvas{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	match event{
	    Event::WindowEvent(WindowEvent::MouseClick{button, state}) => {
		match (button, state){
		    (MouseButton::Left, KeyState::Pressed) => {
			//Check if we clicked outside, in that case cancel overlay
			if let Some((overlay_location, _id, _overlay)) = &self.active_overlay{
			    let overlay_area = Area{
				from: *overlay_location,
				to: *overlay_location + OVERLAY_EXTENT
			    };

			    if !overlay_area.is_in(location){
				//Need to close
				self.overlay_action = OverlayAction::Close;
			    }
			}
		    },
		    _ => {}
		}
	    }
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::LShift, state})) => {
		if *state == KeyState::Pressed{
		    self.is_mod = true;
		}else{
		    self.is_mod = false;
		}
	    },
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::A, state: KeyState::Pressed})) => {

		if self.is_mod{
		    //If already open, close, otherwise open
		    if self.active_overlay.is_some(){
			self.overlay_action = OverlayAction::Close;
		    }else{
			self.overlay_action = OverlayAction::Open(*location);
		    }
		}
		//self.inner_canvas.add_node_at(Mix::into_ui_node(), *location, (10.0, 10.0).into());
	    },
	    //TODO close overlay if clicking somewhere else.
	    _ => {}
	}
	
	//if there is a overlay, push into overlay
	if let Some((overlay_location, _id, overlay)) = &mut self.active_overlay{
	    overlay.on_event(
		&event.offset(*overlay_location * -1.0),
		&(*location - *overlay_location)
	    );
	}
	
	//Push down into canvas
	self.inner_canvas.on_event(event, location);


	//If there is a node to add, and an active overlay (for the location)
	// add the node and issue remove of interface
	match (self.add_node.lock().unwrap().take(), &self.active_overlay){
	    (Some(node_idx), Some((loc, _id, _overlay))) =>  {
		let node = match (self.creator_store.nodes[node_idx].1)(self.executor.clone()) {
		    Some(node) => node,
		    None => {
			warn!("Failed to create node");
			return;
		    }
		};
		self.inner_canvas.add_node_at(node, *loc, (10.0, 10.0).into());
		println!("Added node");
		//Since added, issue removal of the overlay
		self.overlay_action = OverlayAction::Close;
	    },
	    _ => {}
	}
    }

    fn area(&self) -> Area {
	self.inner_canvas.area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls {
	let mut calls = self.inner_canvas.get_calls(host_extent, draw_time, interface, level);

	//if we have an active overlay, draw on top.
	match self.overlay_action.take(){
	    OverlayAction::Open(at) => {
		let interface_id = interface.register_overlay();
		self.active_overlay = Some((at, interface_id, CreatorOverlay::from_store(&self.creator_store, self.executor.clone(), self.add_node.clone())));
	    },
	    OverlayAction::Close => {
		//Deregister overlay and drop it as well
		if let Some(old_ovelay) = self.active_overlay.take(){
		    interface.remove_overlay(old_ovelay.1);
		}
	    },
	    OverlayAction::None => {}
	}


	//If an overlay is still active, render it based on the position it was created for
	if let Some((location, overlay_id, overlay)) = &mut self.active_overlay{
	    let mut overlay_calls = overlay.get_calls(OVERLAY_EXTENT, draw_time, interface, 0);

	    //Offset to location
	    overlay_calls.offset(*location);
	    //clip into global area
	    overlay_calls.clip_to(self.inner_canvas.area().extent());
	    //add to other calls
	    calls.add_overlay_calls(*overlay_id, overlay_calls);
	}
	
	calls
    }
}
