use std::time::Instant;

use neith::{base::Void, com::MutVal};
use neith::event::Event;
use neith::io::Button;
use neith::layout::Direction;
use neith::layout::Split;
use neith::layout::StaticList;
use neith::layout::split::SplitType;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::widget::DrawCalls;
use neith::widget::Widget;
use neith::event::KeyState;
use neith::event::MouseButton;
use neith::event::WindowEvent;

pub mod node_canvas;
mod config_editor;
use config_editor::ConfigOverlay;

const CONFIG_EXT: Vector2<Unit> = Vector2{x: 1200.0, y: 500.0};
const CONFIG_LOC: Vector2<Unit> = Vector2{x: 0.0, y: 0.0};
const CONFIG_AREA: Area = Area{
    from: CONFIG_LOC,
    to: Vector2{
	x: CONFIG_LOC.x + CONFIG_EXT.x,
	y: CONFIG_LOC.y + CONFIG_EXT.y
    }
};

#[derive(Clone, Eq, PartialEq)]
enum ConfigAction{
    Close,
    Open,
    None
}

///Main interface of the program. Layouts the main node canvas as well as the top and lower bar.
pub struct Interface{
    ///Main layout
    layout: Split,

    config_overlay: Option<(usize, ConfigOverlay)>,
    
    config_action: MutVal<ConfigAction>,
}

impl Interface{
    pub fn new() -> Self{

	let config_action = MutVal::new(ConfigAction::None);
	let cscp = config_action.clone();
	
	let layout = Split::new()
	    //Top bar
	    .with_one(
		Split::new()
		    //Main buttons
		    .with_one(
			StaticList::new(Direction::Horizontal)
			    .with_items(vec![
				Box::new(Button::new("Save")),
				Box::new(Button::new("Save As")),
				Box::new(Button::new("Open")),
				Box::new(Button::new("Config").with_action(move || {
				    *cscp.get_mut() = ConfigAction::Open;
				})),
				Box::new(Button::new("Quit")),
			    ])
		    )
		    //Metrics
		    .with_two(Void::new())
		    .with_direction(Direction::Vertical)
		    .with_split_type(SplitType::Relative(0.75))
	    )
	    //Canvas
	    .with_two(
		node_canvas::DagnCanvas::new()
	    )
	    .with_direction(Direction::Horizontal)
	    .with_split_type(SplitType::Absolute(50.0));

	Interface{
	    layout,
	    config_overlay: None,
	    config_action
	}
    }
}

impl Widget for Interface{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	self.layout.on_event(event, location);

	if let Some((_id, overlay)) = &mut self.config_overlay{
	    overlay.on_event(
		&event.offset(CONFIG_LOC * -1.0),
		&(*location - CONFIG_LOC)
	    );
	    //if there was a click somewhere else then the config location, check if we should close
	    // the configurator

	    match event{
		Event::WindowEvent(WindowEvent::MouseClick{button, state}) => {
		    match (button, state){
			(MouseButton::Left, KeyState::Pressed) => {
			    if !CONFIG_AREA.is_in(location){
				*self.config_action.get_mut() = ConfigAction::Close;
			    }
			},
			_ => {}
		    }
		},
		_ => {}
	    }
	}

    }

    fn area(&self) -> Area {
	self.layout.area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &neith::Interface, level: usize) -> DrawCalls {

	
	match *self.config_action.get(){
	    ConfigAction::Open => {
		//check if we don't have any overlay
		if self.config_overlay.is_none(){
		    let new_id = interface.register_overlay();
		    self.config_overlay = Some((new_id, ConfigOverlay::new()));
		} // else drop action
	    },
	    ConfigAction::Close =>{
		if let Some((old_id, _overlay)) = self.config_overlay.take(){
		    interface.remove_overlay(old_id);
		}
	    },
	    _ => {}, //do not have to handle anything
	}
	*self.config_action.get_mut() = ConfigAction::None;
	
	let mut calls = self.layout.get_calls(host_extent, draw_time, interface, level);

	//if overlay is active, add
	if let Some((id, overlay)) = &mut self.config_overlay{
	    let mut overlay_calls = overlay.get_calls(
		CONFIG_EXT,
		draw_time,
		interface,
		0
	    );

	    overlay_calls.offset(CONFIG_LOC);
	    overlay_calls.clip_to(host_extent);
	    
	    calls.add_overlay_calls(*id, overlay_calls);
	}

	calls
    }
}
