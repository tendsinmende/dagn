# Node Ideas
Collects several node ideas. If you started working on one of those, either change this document, or write it somewhere in a board message on gitlab.

If you have a new idea, just add it at the bottom like the others.


# BadSpeaker node
A node that changes a AudioBuffer in a way that it sound like some normal speaker. Should make it possible to check the output on studio monitors as if it was played on some normal consumer speaker.

# WaveTable
WaveTable node or WaveTable synthesizer. Nothing special yet, however crucial parameters should be exposed as ports to make it highly configurable.

# FM Synthesizer
While one could create an FM synthesizer only via Sinus and value nodes, a node could be implemented faster and more memory efficient.

# Envelope
For midi signals and value. In essence, you'd have a midi input as well as the clock. The output is the value 1.0 modulated by the filter. This value can then be used for anything. Not just volume.

# Cpal in/output
Cpal is a more crossplatform crate that provides audio in/output as well. This would also add the benefit of possibly adding asio support on windows.
