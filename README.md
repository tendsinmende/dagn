# DAGn
A sound focused realtime graph framework. 

## Description
TODO some nice description what this is.

## Building
### Requirements
- More or less beefy CPU with > 4 physical cores. Otherwise you might get artefacts while running.
- Vulkan capable graphics card
- Some recent form of GNU/Linux (windows/mac might work but is untested)
- Jack Sound server

### Building from source
You'll need Rust `cargo` installed on your system. The easiest way is to follow [this](https://rustup.rs/) guide for your system.

### Running

First of all make sure that the Jack sound server is running.

Now change into the directory and call:
```bash
cargo run
```
this will build and run the tool.

You can add a JackInput over a Sinus to a JackOutput. Then connect the Dagn output wihtin your jack sound server to an audio out. You should now hear a sinus sound playing.
The graph should look like this:

![sample_graph](media/sample_graph.png)

## Implementing new nodes
Have a look at the already implemented nodes. Then either work on your own or pick one of the nodes [here](media/node_ideas.md).

## License
The whole project is release under the [MPL license](https://www.mozilla.org/en-US/MPL/).

All contributions will be licensed under it as well.
